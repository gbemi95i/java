import java.util.List;
import java.util.ArrayList;
public class RosterQuery{
	
	public static void main(String[] args){
		List<Staff> roster=new ArrayList<Staff>();
		String[] fName={"David", "James", "Nike", "Bosco", "Pius", "Emeka", "Jide", "Ebuka", "Chibuzor", "Babatunde", "Efe", "Eben", "Martins", "Esther", "Barbara", "Anita", "Ife", "Ngozi", "Ebere"};
		String[] lName={"Iwobi", "Adebayo", "Oluwadara", "Enemke", "Efowore", "Njike", "Williams", "Anietoh", "Ndube", "Raji", "Coker", "Bassey", "Obafemi", "Fabamise", "Schmidt", "Kate", "Olubodun", "Enere", "Odinma"};
		
		roster.add(new Staff("David", "Iwobi", 21, Staff.department.IT, Staff.gender.MALE));
		roster.add(new Staff("James", "Adebayo", 22, Staff.department.IT, Staff.gender.MALE));
		roster.add(new Staff("Nike", "Oluwadara", 23, Staff.department.IT, Staff.gender.FEMALE));
		roster.add(new Staff("Bosco", "Enemke", 24, Staff.department.MARKETING, Staff.gender.MALE));
		roster.add(new Staff("Emeka", "Efowore", 25, Staff.department.MARKETING, Staff.gender.MALE));
		roster.add(new Staff("Jide", "Njike", 21, Staff.department.MARKETING, Staff.gender.MALE));
		roster.add(new Staff("Ebuka", "Williams", 22, Staff.department.HR, Staff.gender.MALE));
		roster.add(new Staff("Chibuzor", "Anietoh", 23, Staff.department.HR, Staff.gender.MALE));
		roster.add(new Staff("Babatunde", "Raji", 24, Staff.department.HR, Staff.gender.MALE));
		roster.add(new Staff("Efe", "Coker", 25, Staff.department.SECURITY, Staff.gender.MALE));
		roster.add(new Staff("Eben", "Bassey", 21, Staff.department.SECURITY, Staff.gender.MALE));
		roster.add(new Staff("Martins", "Obafemi", 22, Staff.department.SECURITY, Staff.gender.MALE));
		roster.add(new Staff("Esther", "Fabamise", 23, Staff.department.ACCOUNTING, Staff.gender.FEMALE));
		roster.add(new Staff("Barbara", "Schmidt", 24, Staff.department.ACCOUNTING, Staff.gender.FEMALE));
		roster.add(new Staff("Anita", "Kate", 25, Staff.department.ACCOUNTING, Staff.gender.FEMALE));
		roster.add(new Staff("Ife", "Olubodun", 21, Staff.department.IT, Staff.gender.FEMALE));
		roster.add(new Staff("Ngozi", "Enere", 22, Staff.department.IT, Staff.gender.FEMALE));
		roster.add(new Staff("Ebere", "Odinma", 23, Staff.department.IT, Staff.gender.FEMALE));
		
		
		roster.stream()
		.filter(e -> e.gend==Staff.gender.FEMALE && e.dept==Staff.department.ACCOUNTING)
		.forEach(e->System.out.println(e.getFullName()));
		
		//System.out.println(roster.get(0).dept);
	}
}