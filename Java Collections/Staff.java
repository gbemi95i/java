class Staff{
	String fName;
	String lName;
	int age;
	enum gender{
		MALE, FEMALE
	};
	enum department{
		IT, MARKETING, HR, SECURITY, ACCOUNTING
	}
	
	department dept;
	gender gend;
	
	Staff(String fName, String lName, int age, department d, gender g){
		this.fName=fName;
		this.lName=lName;
		this.age=age;
		this.dept=d;
		this.gend=g;
	}
	
	String getFullName(){return fName+" "+lName;};
	String getfName(){return fName;};
	String getlName(){return lName;};
	int getAge(){return age;};
	department getDept(){return dept;};
}