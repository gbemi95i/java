import java.util.List;
import java.util.ArrayList;
public class ListTesting2{
	public static void main(String[] args){
		List <String> l=new ArrayList<String>();
		l.add("5");
		l.add("bla");
		l.add("true");
		
		for(String i : l){
			System.out.println(i);
		}
		
		String[] s=l.toArray(new String[0]);
		
		for(String i : s){
			System.out.println(i);
		}
	}
}