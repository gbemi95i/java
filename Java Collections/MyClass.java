import java.util.Set;
import java.util.HashSet;

public class MyClass { 
  public static void main(String[] args) { 
    Set<String> cars = new HashSet<String>();
    cars.add("Volvo");
    cars.add("BMW");
    cars.add("Ford");
    cars.add("Mazda");
    System.out.println(cars.get(4));
  } 
}