import java.util.List;
import java.util.ArrayList;
public class ListTesting{
	public static void main(String[] args){
		List <Object> l=new ArrayList<Object>();
		Object[] m=new Object[3];
		l.add(5);
		l.add("bla");
		l.add(true);
		
		m[0]=5;
		m[1]="bla";
		m[2]=true;
		
		int[] n={2,3,4};
		
		int[] p=new int[]{5,6,7};
		
		/*for(Object i : l){
			System.out.println(i);
		};
		
		System.out.println("------------------------");
		
		for(Object i : m){
			System.out.println(i);
		}
		
		System.out.println("------------------------");*/
		
		for(int i:n){
			System.out.println(i);
		}
		
		System.out.println("------------------------");
		
		for(int i:p){
			int k=555;
			System.out.println(i);
		}
		System.out.println("------------------------");

		//System.out.println(k);
		String q="XV";
		System.out.println(q);
	}
}