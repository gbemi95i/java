import java.util.Set;
import java.util.HashSet;
import java.util.LinkedHashSet;
public class StringTest{
	public static void main(String[] args){
		//Set<Person> s2=new LinkedHashSet<Person>();
		
		Person p1=new Person("David", "Olalekan");
		Person p2=new Person("David", "Olalekan");
		Person p3=p1;
		
		// String s1=p1.getFullName();
		// String s2=p2.getFullName();
		// String s3=s1;
		
		String s1=new String("aaa");
		String s2=new String("aaa");
		String s3=s1;
		
		System.out.println(p1==p2);
		System.out.println(p1==p3);
		System.out.println(p1.equals(p2)); //THE EQUALS METHOD FOR OBJECTS IS MORE THOROUGH...AS THOROUGH AS '=='
		System.out.println(p1.equals(p3));
		
		System.out.println(s1==s2);
		System.out.println(s1==s3);
		System.out.println(s1.equals(s2));
		System.out.println(s1.equals(s2));		
		
		/*s2.add(new Person("David", "Olalekan"));
		s2.add(new Person("Lanre", "Omoseyin"));
		s2.add(new Person("Wasiu", "Fabamise"));
		
		for(Person p : s2)
			System.out.println(p.getFullName());
		
		System.out.println("------");
		
		s2.add(new Person("Lanre", "Omoseyin"));
		for(Person p : s2)
			System.out.println(p.getFullName());*/
	}
}