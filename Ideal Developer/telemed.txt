-Socket issue
	-Have users ping server at intervals
	-Upon inactivity after said interval, 	 render meeting as ended with end type as 	 abruptly.
	-If meeting is being accessed, have 	 isValid method check for meeting state 	 and end_type







medclient@imacious.com
password




-Setting password requires password!
	-I suggest moving the password setting option to the auth servlet and 	 creating a validity period for links as well as encoding.
-Seems a third person can join
-Patient is connecting with doc in meeting even when the meeting id is different. There should be a confirmation first.
	-There either needs to be a meeting identifier id for both valid 	 	 particpants or validation should only return true if that 	 meeting is the most recent.
-Case where user has already deposited
	-Send link or alter payment button behaviour before rendering
TO ADD TO JIRA:
	-Add error page
	-Prompts(yes or no) and message (TelemedManager).








-Setting up Jenkins
	-Set up a jenkins branch for testing	./
	-Install Jenkins	./
	-Link jenkins with bitbucket	./
	-Access project in .jenkins	./
	-Try building from there	./
-Add all dependencies using repo style and run	./
-Try adding a servlet and run	./
-Add all servlets and run	./
-Add web folder and run!	./
-Try open webSocket from app.js		./
-Make messages component specific	./
-Clear listeners upon unmount		./
-Implement Task.java in Telemed	./
	-Imitate how Task.java is being extended in VisitMonitor.java
-Fix local video weird location before remote connects	./
-db keys    ./
	-Save to db    ./
	-Decide when to be fetched    ./
	-Create DS command    ./
	-Fetch    ./
-Add user@example user	./
	-Observe login behaviours
	-Create user with PROF usertype
	-Test
-refNum should be verified through paystack and it must be for the right user and unrendered  ./
-THE MOMENT A USER HAS LOGGED IN... HE CAN JOIN QUEUE OR START  MEETING WITH A URL. THIS IS BAD   ./
-MeetingObject
	-Set up a meeting object creation  ./
	-Link it with invite sent. Send is also  ./
	-Start meeting upon acceptance.  ./
	-Update meeting upon ending  ./
-Work on return types  ./
-getUserPaymentHistory and getOrgEarningHistory outside manager   ./
-Move maps or list of maps methods like fetchClientQueueInfo   ./
-The nested sql operations should be changed to passing list of ids   ./
-Prof note should have:  ./
	-profId
-Meeting entity should be recorded  ./
	-Meeting table:		./
		-id,profId,clientId,timeStarted,timeEnded,End type...abruptly 		 	 or good,noteAddress
-Save meeting event if ended appropriately with noteAdd,timeEnded as null and  endType as ABRUPTLY    ./
-Doctor's financial report ./
-queue order issue ...asc ./
-My payments 45mins./
	-Design UI first hardcodedly ./
	-Code programmatic sequence ./
	-Create service_item table: ./
		-id
		-name
		-price
		-ord_id
	-Add ServiceItem class and manager./
	-Test input and fetch ./
	-Implement in paymentManager's fetch payment history ./

-Billing servlet 15mins ./
	-Copy user servlet and change names with notepad ./
	-Delete all classes except billing related ones. ./

-Update Jira 10 mins
	-Queue order issue...asc

-Table:					./
	-id
	-ref_num
	-service_id
	-client_id
	-org_id
	-date_time
	-amount_paid(Check aquila for money value format)
	-service_rendered

-Saving data:                           ./
	-Payment and paymentManager
-if user needs to login, the page shouldn't have loaded already ./
-Let the doctor know an invite has been seen ./
-video size issue ./
-Only the doctor should be able to full end a meeting ./
-Add the following fields: ./
	-note
	-diagnosis
	-investigation
	-prescription
-Update Jira ./
-Add loader before requests ./
-Doc shouldn't receive refresh-queue from server during meeting! ./
-Meeting onMessage should receive refresh queue command and even if it 	does...the response is wrong ./
-Access denied went to dashboard ./
-Watch timer object after countdown
-refreshing queue and some other commands demanding only userId  shouldn't need a parameter that's what getCurrentUser method is for ./
-Estimated time is the same irrespective of client queue position ./

-Refresh queue after countdown....needs queue id ./
	-Add active professionals table...(queueId, profId, active) ./
	-Concrete getQueueActiveDocsMethod ./
	-Implement in sockets with cases where no doc is available./
-Only people with a particular queue id ./
	-Capture the right fields and methods of sockets (static)
	-Receive queueId ./
	-Send refresh-queue command to the members of the queue ./
	- on receiving message reload queue ./
-Access denied for patient after call end......went to waiting room./
-black screen after other participant reloads ./
-Get patient's correct age and gender for doc./
-Add date created./
-function call for next patient
-When meeting ends:
	it sends data to the server side from doc's side for saving stuff and 	refreshing queue and returns to the waiting room to call for next 		patient thereby changing new-patient state for re-rendering change.


-Password should be set via the link sent to the person's mail.
-USER CAN'T SEEM TO REJOIN AFTER MEETING AGAIN (REDIRECTING BACK ISSUE)
-SORT QUEUE QUERY BY DATE

-GO THROUGH HASHING AND SALTING

-RELOAD QUEUE SHOULDN'T APPLY TO NEW QUEUE MEMBER.

-Discuss past visits view for doc
-Discuss past earnings view for doc
-Past payments for patient




















DONE:
-SOCKET LAST TIME ONLINE PING
	-Make sockets are removed from lists after closure./
	-Upon closure check if there is any socket open./
		-If not start timer to close socket if last socket closed in n minutes away from the timer's end./
			-If n-minutes away: remove user email for map./

-PLAN REGISTRATION FOR CLIENTS BY ORGANISATION FUNCTIONALITY-be done by 12:30
	-Fields required: first & last name, email, dob,gender./
	
-PLAN ADDING HTTP SESSIONS FOR DS FUNCTIONALITY./	

-DECIDE PLACES TO CHECK USER TYPE AFTER GET CURRENT USER TO AVOID PATIENT ENTERING DOCTOR'S  CONSOLE WITH URL./

-PLAN UPON REJECTING SENDING RESPONSE TO DOC AND ACT ACCORDINGLY./


TALK TO IMACIOUS ABOUT SET FOR ONCLOSE ITERATION IN SOCKETS./
There should be method entries of queue given the id of the queue
