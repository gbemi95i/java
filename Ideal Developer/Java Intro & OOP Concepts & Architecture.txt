What is a low level language?
A low level language is any language that is considered close to machine instructions. They usually have little or no abstraction which makes they difficult to read, so they are not human readable like machine language, assembly languages like ibm 360 assembly language.


What is a high level language?
A high level language is a language that has a lot of abstraction at least when compared to low level languages, they usually have libraries and resources built into them which makes them human readable and easier to use.


What is Java?
Java is a high level programming, object oriented, class based programming language. As I said object oriented, it implies that most oop concepts like inheritance and polymorphism applies to it. It is platform independent which implies that with jdk java can run on almost any machine.


Why is Java considered class based?
Well class based programming is a paradigm under OOP but the key difference and why Java is considered a class based programming language is in the way objects are created. In Java, Objects are instances of a class. Classes as the bluebrint for an object unlike pure OOP languages like SmallTalk where objects are created using the new method. Classes aren't so important, virtually every accessible entity is an object.


Why is Java considered platform independent?
Java is considered platform independent because with jdk, java can run on almost any machine. It has it's own compiler javac that converts java code to byte in the JIT compiler in jvm compiles the byte code to the machine's native language without any dependence on external resources.


What’s role of JVM in making java a platform independent language?
JVM has the JIT(Just In Time) compiler that converts byte code to the machine's native language thereby making the program excutable without any need for external tools or resources.

What is JVM in java?
JVM is arguably the most vital part of the jdk (Java Development Kit), particularly because it is responsible for byte code execution, either by compiling byte code to the machine native language...making it executable or executing byte code line by line using an interpreter within.

Know the following components of JVM?
-JIT compiler: It responsible for compiling Java's byte code to the machine's native language.

-Heap: Java heap is the area of memory used to store objects instantiated by applications running on the JVM.

-Garbage Collector: The garbage collector finds these unused objects and deletes them to free up memory. When the heap becomes full, the garbage collector clears it.
It also runs after a program has ended.


What is Differences between JDK, JRE and JVM?
JDK=tool like javac + JRE
JRE=Libraries and enviroment + JVM
JVM=JIT compiler, Heap, Garbage Collector.




OOP Concepts
Abstraction and why?
Abstraction is a concept of oop language that helps to hide how objects operate only revealing that they do...that is the result. So it hides all the complexies of a block of code while allowing the actual use of that code.
Abstraction is a process of hiding the implementation details and showing only functionality to the user.
Example: AquilaServerGUI's dbConnector.backupTo(backupFile);



Data Encapsulation and why?
Data Encapsulation is a feature in OOP that enables a developer set boundaries as to how much manipulation can do done on a field or entity in code to avoid compromising the nature the behaviour of that field.
For instance, you could have a field that you don't want users to manipulate so you set that field as private and have create only a get method for accessing that field


Polymorphism and why?
Polymorphism is a phenomenon in OOP where an object can inherit properties from a parent class or could be an interface and yet have different or unique properties of it's own.
Remember a Bicycle object called bmx with an extra property called gear.
There are numerious ways of polymorphism could occur:
-You could have a class(mikano) extend another class(generator) and yet implement an interface(engine)
-Method overriding & method overloading also help achieve polymorphism.


Inheritance and why?
Inheritance is a very common phenomenon in OOP where a object naturally inherits the properties of it's parent.



Application Architecture:
Is process that defines the structure and behaviour of an application, basically how each components interacts with other components.
Depending on the kind of application and the perspective, there numerous software architectural patterns(layered architecture, client-server pattern, event driven architecture). In layered architecture an application could be a 3-tier or 4-tier architecture application. Such architecture is referred to as n-tier architecture


N-TIER ARCHITECTURE / LAYERED ARCHITECTURE:
This a type of software architecture where all objects and resources are grouped into 3 or 4 layers. This structure is simple and orderly since changes could be made in made in a layer without directly affecting the other layers. It offers a degree of security to integral parts of the code.



-Presentation layer:
It handles the UI of the application. It is resposnsible for displaying information received from the server, It also contains elements like forms that are used to initiate a request to the server.
This layer contains ui resources like html, css, swing, jsp, servlets and so on.



-Business layer
The business is where the application's logic resides. This is the part where data is processed, manipulated or converted into whatever desired form. It is the link between the UI and the layer where SQL presides so a lot of times it is either preparing data to be saved or preparing data for client or the execution of a recurrent task and so on. Majority of the java code would reside here if its a java application.
It contains the business objects in form of entities and so on.
In come cases the business layer is actually merged with the persistence layer to form a 3 tier architecture system: presentation layer, business login layer, data access layer

BO: A business object is an object that can be used to different of things...I have used them for entities. Business objects are used to hold a set of instance properties and variables that usually represent an item in the database.
For instance, you could have a BO instance called currentUser. The all variables in that instance would only apply to the that BO instance because it would probably represent an actual user in the database.
So the DAO is commonly used to save or retrieve BO instances as entities.

Entity: An entity is an object that represents a table in the database.
	Each instance would represent a row in that table

Domain class/model object/entity (used interchangeably)



-Persistence layer
It is the layer responsible for accessing the database. It is where the DAO objects resides. This is where the dbms or rdbms language and other data persistence tools like ORM like hibernate are implemented.

The DAO pattern often in this layer. It is a structural pattern that allows us to isolate the business layer from the persistence layer.

DAO: Data Access Object restricts access to the data source, it helps to isolate the persistence layer. This is very helpful in preventing any change in the persistence layer from affection the business layer or any other layer.
For instance, you have a DAO called userManager that extends an ORM class. An instance of this userManager can perform CRUD operations on a users table with the appropriate methods without affections the other layers that use thes methods.



-Database layer
This is where the actual data is stored


Developers practicing continuous integration merge their changes back to the main branch as often as possible. By doing so, you avoid the integration hell that usually happens when people wait for release day to merge their changes into the release branch.

Continuous delivery is an extension of continuous integration to make sure that you can release new changes to your customers quickly in a sustainable way. This means that on top of having automated your testing, you also have automated your release process and you can deploy your application at any point of time by clicking on a button.

Continuous deployment goes one step further than continuous delivery. With this practice, every change that passes all stages of your production pipeline is released to your customers. There's no human intervention, and only a failed test will prevent a new change to be deployed to production.