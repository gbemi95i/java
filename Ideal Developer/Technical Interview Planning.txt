JAVA INTRO		3 days		./
jdk structure, behaviour & characteristics, jre, jvm



JAVA & OOP CHARACTERISTICS & CONCEPTS	7 days		./
Among other things remember dao, dto, bo, vo concepts ... there's a 	 bookmark in Java



JAVA WEB	10 days
Servlets, filters, Servlet vs jsp,sockets, protocol, stateful vs stateless protocol, tcp vs udp, http vs websockets***, RESTful, websockets vs RESTful, ssl...symmetric vs asymmetric.



ORM & HIBERNATE		2 days


SQL		2 days
aside from the syntax...remember dbms,rdbms,no-sql,sql


GIT...BITBUCKET		2 days



AWS		2 days
Setup, UBUNTU common commands,**check "to internalize" for terms**


JENKINS		2 days
Setup and pipeline

JIRA		2 days


Terms		3 days
Try to link them to your experience if possible
	-SDLC...Agile Model and others
	-DevOps
	-Microservices


JAVA CODE



JAVA PROJECT STRUCTURE AND BUILD ENGINES 2.5 weeks
	-Understand how build automation works
	-Understand build.xml and co
	-Create a maven project
	-Convert Telemed to maven
	-Take note of key differences between them