public class arrays2{
	public static void main(String[] args){
		String[] numbers={"2","4","6","8"};
		//JAVA ARRAYS ARE IMMUTABLE I.E. THE SIZE CANNOT BE ALTERED AFTER DECLARATION.
		int[] numbers2=new int[4];
		for(int i=0; i<numbers.length; i++){
			System.out.println(numbers[i]);
			int j=Integer.parseInt(numbers[i]);
			numbers2[i]=j;
		}
		
		System.out.println("These ones are numbers + 1:");
		for(int k:numbers2){
			System.out.println(k+1);
		}
	}
}