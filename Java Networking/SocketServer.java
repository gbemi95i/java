import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.Scanner;
public class SocketServer{
	public static void main(String[] args){
		int portNumber=80;
		try( 
			ServerSocket serverSocket = new ServerSocket(portNumber);
			Socket clientSocket = serverSocket.accept();
			PrintWriter out =
				new PrintWriter(clientSocket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(
				new InputStreamReader(clientSocket.getInputStream()));
		){
			String inputLine, outputLine;
			while((inputLine=in.readLine())!=null){
				System.out.println("Client:"+inputLine);
				if(inputLine.equals("Bye.")){
					break;
				}
			}
		}catch(IOException i){
			System.err.println(i);
		}
	}
}