import java.io.*;
import java.util.ArrayList;
public class FileAction implements FileActionsInter{
	
	public void copyFile(File src,File dst){
		
		//COPY FILE
		try(FileInputStream in=new FileInputStream(src);
			FileOutputStream out=new FileOutputStream(dst)){
			copyStreamToStream(in,out);
		}catch(IOException e){
			e.printStackTrace();
		}//YOU RUN THE RISK OF OPEN STREAMS (CATCH VS THROWS IN METHODS SIGNATURE)
	}

	public void moveFile(File src,File dst){
		//FileInputStream in=null;
		//FileOutputStream out=null;
		
		//COPY FILE & DELETE OLD
		try(FileInputStream in=new FileInputStream(src);
			FileOutputStream out=new FileOutputStream(dst)){
			copyStreamToStream(in,out);
		}catch(IOException e){
			e.printStackTrace();
		}
		src.delete();
	}
	
	public void renameFile(File file, String newName){
		File newRenamedFile=new File(newName);
		//RENAME FILE
		file.renameTo(newRenamedFile);
	}
	
	public void validFile(File src, String name){
		/**if(checkFile(src)){
			String p=src.toAbsoluteFile().getParent()+"/"+name;
			File newFile=Files.get(p);
			try{
				Files.move(src,newFile);
				System.out.println(src.getFileName()+" renamed to "+name);
			}catch(IOException e){
				System.err.println("IO Error: "+e);
			}
		}else{
			System.err.println("File Error: File/file directory is invalid! "+src);
		}**/
	}


	public boolean checkFile(File file){return true;};
	
	public boolean containsString(File file, String sampleString){
		String content="";
		
		//EXTRACT FILE CONTENT
		try(FileInputStream in=new FileInputStream(file)){
			int b;
			int i=0;
			while((b=in.read()) != -1){
				char c = (char) b;
				content=content+c;
			}
		}catch(IOException e){
			System.err.println(e);
		}
		
		//System.out.println(content);
		//boolean result=isFound(content,sampleString);
		
		//CHECK IF CONTENT CONTAINS STRING
		return content.contains(sampleString);
	};
	
	public int firstOccurenceInFile(File file, String sampleString){
		String content="";
		try(FileInputStream in=new FileInputStream(file)){
			int b;
			int i=0;
			while((b=in.read()) != -1){
				char c = (char) b;
				content=content+c;
			}
		}catch(IOException e){
			System.err.println(e);
		}
		
		//System.out.println(content);

		//CHECK IF CONTENT CONTAINS STRING
		if(!content.contains(sampleString)){
			return -1;
		}else{
			//System.out.println(content.indexOf(sampleString));
			//CHECK STRING FIRST INDEX
			return content.indexOf(sampleString);
		}
	};
	
	public int lastOccurentInFile(File file, String sampleString){
		String content="";

		//EXTRACT FILE CONTENT
		try(FileInputStream in=new FileInputStream(file)){
			int b;
			int i=0;
			while((b=in.read()) != -1){
				char c = (char) b;
				content=content+c;
			}
		}catch(IOException e){
			System.err.println(e);
		}
		
		//System.out.println(content);

		//CHECK IF CONTENT CONTAINS STRING
		if(!content.contains(sampleString)){
			return -1;
		}else{
			//System.out.println(content.lastIndexOf(sampleString));
			//CHECK STRING FIRST INDEX
			return content.lastIndexOf(sampleString);
		}
	};
	
	public void deleteFile(File file){
		if(file.isDirectory()){
			File[] files = file.listFiles();
			
			for(File x: files){
				if(x.isDirectory()){
					//System.out.println(x.listFiles().length);
					if(x.listFiles().length!=0){
						String str=x.getAbsolutePath();
						File files2=new File(str);
						deleteFile(files2);
						x.delete();
					}else{
						//System.out.println(x.getName());
						x.delete();
					}
				}else{
					x.delete();
				}
				//System.out.println(x.getName());
			}
		}else{
			file.delete();
		}
	};
	
	public File[] findFilesWithString(File directory, String sampleString){
		ArrayList<File> fileList = new ArrayList<File>();
		for(File x : directory.listFiles()){
			if(containsString(x, sampleString)){
				fileList.add(x);
			}
		}
		
		int len=fileList.size();
		//System.out.println(len);
		File[] files=new File[len];
		files=fileList.toArray(new File[len]);
		//System.out.println(files.length);
		// System.out.println(listOfFiles.length);
		// System.out.println(directory.getName());
		
		return files;
	};
	
	public void copyStreamToStream(InputStream in, OutputStream out) throws IOException{
		//COPY FILE
		int c;
		byte[] b=new byte[10000];
		//int len=1000;
		
		//while((c=in.read(b,0,len)) != -1){
		//IOException ioe=new IOException();
		/**if(true){
			throw ioe;
		}**/
		while((c=in.read(b)) != -1){
			out.write(b,0,c);
			//System.out.println("Did it");
			/**THE WRITE METHOD TAKES THE INT AND WRITES THE BYTE
				VALUE TO THE OUTPUT STREAM**/
		}
	}

	public String streamToString(InputStream in) throws IOException{
		byte[] b=streamToBytes(in);
		
		String s=new String(b);
		
		return s;
	}


	public byte[] streamToBytes(InputStream in) throws IOException{
		//COPY FILE
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		
		copyStreamToStream(in, baos);
		
		return baos.toByteArray();
	}
}