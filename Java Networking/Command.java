import java.io.*;
import java.util.Scanner;
public class Command{
	
	public static void execCommand(String cmd){
        //checkCommandValidity(cmd);
		
		//IF COMMAND IS VALID GO ON
        //if(checkCommandValidity(cmd)){
			System.out.println("Valid");

			//GET COMMAND ELEMENTS
			CommandHandler handler = new CommandHandler();
			String[] keys = handler.extractCommandSchema(cmd);
			
			//GROUP COMMAND BY STRUCTURE(2 OR 4 SCHEME ELEMENTS (E.G.: 'COPY FILE TO FILE' (4) VS DELETE FILE (2))
			int l=keys.length;
			if(l==4){
				String action=keys[0];
				String from=keys[1];
				String loc=keys[2];
				String to=keys[3];
				
				File src=new File(from);
				File dst=new File(to);
				//FileActionTester tester=new FileActionTester();
				
				int result=1;
				if(action.equals("copy")){
					//boolean result=tester.testCopier(src,dst);
					System.out.println(result);
				}else if(action.equals("move")){
					//boolean result=tester.testMover(src,dst);
					System.out.println(result);
				}else if(action.equals("rename")){
					//boolean result=tester.testRenamer(src,dst);
					System.out.println(result);
				}else if(action.equals("find")){
					//boolean result=tester.containsString(src,to);
					System.out.println(result);
				}else if(action.equals("firstoccurence")){
					//int result=tester.firstOccurenceInFile(src,to);
					System.out.println(result);
				}else if(action.equals("lastoccurence")){
					//int result=tester.lastOccurenceInFile(src,to);
					System.out.println(result);
				}
			}else{
				/**String action=keys[0];
				String f=keys[1];
				File file=new File(f);
				if(file.isDirectory()){
					FileActionTester tester=new FileActionTester();
					tester.emptyDir(f);
					file.delete();
				}else{
					file.delete();
				}
				System.out.println("Deleted");*/
			}
			
		//}else{
			//System.out.println("Please check your command properly.");
		//};
		//System.out.println(cmd);
		/**Scanner s = new Scanner(cmd);
		while(s.hasNext()){
			String x=(String) s.next();
			System.out.println(x);
		}**/
	}
	
	public static boolean isInstruction(String cmd){
		boolean valid=true;
		
		//USE COMMANDHANDLER OBJECT TO HANDLER RETRIEVE SCHEMA ELEMENTS
		CommandHandler handler = new CommandHandler();
		String[] keys = handler.extractCommandSchema(cmd);
		int l=keys.length;
		
		/*for(int i=0; i<l; i++){
			System.out.println(keys[i]);
		}*/
		if(l==4){
			String action=keys[0];
			String from=keys[1];
			String loc=keys[2];
			String to=keys[3];
			
			File f;
			
			if(action.equals("copy") || action.equals("move") || action.equals("rename") || action.equals("find") || action.equals("firstoccurence") || action.equals("lastoccurence")){
				f=new File(from);
				
				if(!f.isFile() && !f.exists()){
					valid=false;
					//System.out.println("Please check your command properly.");
				}else{
					if(!loc.equals("in") && !loc.equals("to")){
						valid=false;
						//System.out.println("Please check your command properly.");
					}else{
						valid=true;
					}
				}
			}else{
				valid=false;
				//System.out.println("Please check your command properly.");
			}
		}else if(l==2){//ONLY THE DELETE COMMAND FALLS UNDER THE 2-SCHEMA GROUP
			String action=keys[0];
			String file=keys[1];
			File f;
			
			f=new File(file);
			if(!f.isDirectory() && !f.isFile() && !f.exists()){
				//System.out.println("Please check your command properly.");
				valid=false;
			}else{
				valid=true;
			}
			
		}else{
			valid=false;
			//System.out.println("Please check your command properly.");
		}
		
		return valid;
	}
}