import java.io.*;
import java.nio.file.*;
import java.net.Socket;
import java.util.Scanner;
import java.util.Arrays;
public class ChatClient3{
	static boolean expecting=false;
	static String cmd="";
	
	public static void main(String[] args){
		//String hostName="192.168.8.100";
		String hostName="localhost";
		int portNumber=80;
		
		try(
			Socket echoSocket = new Socket(hostName, portNumber);
			DataOutputStream out =
				new DataOutputStream(echoSocket.getOutputStream());
			DataInputStream in =
				new DataInputStream(echoSocket.getInputStream());
		){
			//Thread l=new Thread(new Listener(out,in));
			Thread r=new Thread(new Reader(out,in));
			//l.start();
			r.start();
			
			Scanner sc=new Scanner(System.in);
			Command cm=new Command();
			String outputLine;
			byte[] output;
			
			while((outputLine=sc.nextLine()) != null){
				if(cm.isInstruction(outputLine)){
					output=outputLine.getBytes();
					
					expecting=true;
					cmd=outputLine;
					out.write(output,0,output.length);
				}else{
					output=outputLine.getBytes();
					out.write(output,0,output.length);
					System.out.println("Me:"+outputLine);
				}
				
			}
		}catch(IOException e){
			System.err.println("ERR 1:"+e);
		}
	}
	
		
	public static boolean assess(byte[] b, int readLength){
		byte[] actualMessage=new byte[readLength];
		int message;
		if(readLength==1){
			message=b[0];
			if(message==400){
				return false;
			}
		}
		
		return true;
	}
	

	public static void writeToDst(byte[] b, int readLength, String inputLine){
		CommandHandler handler = new CommandHandler();
		String[] keys = handler.extractCommandSchema(inputLine);
		String to=keys[3];
		//System.out.println("Dst: "+to);		Path dst=Paths.get(to);
		Path dst=Paths.get(to);
		
		try{
			Files.write(dst, b);
		}catch(IOException e){}
	}
	
	class FileSender{
		public byte[] executeInstruction(String inputLine, DataOutputStream out){
			CommandHandler handler = new CommandHandler();
			String[] keys = handler.extractCommandSchema(inputLine);
			
			String action=keys[0];
			String from=keys[1];
			Path file=Paths.get(from);
			byte[] b=null;
			//System.out.println("Got here");
			
			try{
				b=Files.readAllBytes(file);
			}catch(IOException e){}
			
			return b;
		}
	}
	
	
	
	
	static class Reader implements Runnable{
		DataOutputStream out;
		DataInputStream in;

		Reader(DataOutputStream out, DataInputStream in){
			this.out=out;
			this.in=in;
		}
	
		public void run(){
			int readLength;
			String inputLine;
			String outputLine="";
			byte[] b=new byte[2048];
			byte[] output;
			Command cm=new Command();
			ChatServer2.FileSender sender=new ChatServer2().new FileSender();
			
			try{
				while((readLength=in.read(b))!=-1){
					//HANDLING INPUT/REQUEST/RESPONSE
					inputLine=extractString(b, readLength);
					
					if(inputLine.equals("Bye.")){
						break;
					}else if(cm.isInstruction(inputLine)){
						System.out.println("Server:"+inputLine);
						output=sender.executeInstruction(inputLine, out);
						out.write(output,0,output.length);
						
						readLength=in.read(b,0,b.length);
						inputLine=extractString(b, readLength);
						System.out.println("Server:"+inputLine);
					}else if(expecting){
						readLength=b.length;
						//System.out.println("Got here1");
						if(assess(b,readLength)){
							//System.out.println("Got here2");
							writeToDst(b, readLength, cmd);
							
							outputLine="Received. Thanks";
							output=outputLine.getBytes();
							out.write(output,0,output.length);
							System.out.println("Me:"+outputLine);
							
							expecting=false;
						}else{System.out.println("Bad request");};
					}else{
						System.out.println("Server:"+inputLine);
					}
				}
			}catch(IOException e){
				System.err.println("ERR 3:"+e);
			}
		}
		
		
		public static boolean assess(byte[] b, int readLength){
			byte[] actualMessage=new byte[readLength];
			int message;
			if(readLength==1){
				message=b[0];
				if(message==400){
					return false;
				}
			}
			
			return true;
		}
		
		public static String extractString(byte[] b, int readLength){
			byte[] actualMessage=new byte[readLength];
			String message;
			int i=0;
			while(i!=actualMessage.length){
				actualMessage[i]=b[i];
				i++;
			}
			message=new String(actualMessage);
			
			return message;
		}
	}
}