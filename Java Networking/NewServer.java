import java.io.*;
import java.nio.file.*;
import java.net.Socket;
import java.net.ServerSocket;
public class NewServer{
	public static void main(String[] args){
		int portNumber=8080;
		try( 
			ServerSocket serverSocket = new ServerSocket(portNumber);
			Socket clientSocket = serverSocket.accept();
			DataOutputStream out =
				new DataOutputStream(clientSocket.getOutputStream());
			DataInputStream in =
				new DataInputStream(clientSocket.getInputStream());
		){
			System.out.println("Connected");
			String response = "<html><body><div>Wonderful!!!</div><b>Wow! Okay!!</b></body></html>";
			out.writeBytes("HTTP/1.1 200 OK\r\n");
			out.writeBytes("Content-Length: " +67+ "\r\n\r\n");
			out.writeBytes(response);
			out.flush();
			
			FileAction f=new FileAction();
			
			String s=f.streamToString(in);
			System.out.println("Content is "+s);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
