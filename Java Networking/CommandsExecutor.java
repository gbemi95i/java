import java.io.*;
public class CommandsExecutor{
	public static void main(String[] args){
		int c;
		String cmd=null;
		int i=1;
		try(BufferedReader br = new BufferedReader(new FileReader("commands.txt"))){
			Command executor=new Command();
			while((cmd=br.readLine())!=null){
				System.out.println("Command "+i+":");
				System.out.println(cmd);
				executor.execCommand(cmd);
				i++;
			}
		}catch(IOException e){
			System.out.println(e);
		}
	}
}