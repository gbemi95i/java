import java.io.*;
import java.net.Socket;
public class SocketClient{
	public static void main(String[] args){
		String hostName="http://google.com";
		int portNumber=80;
		try (
			Socket echoSocket = new Socket(hostName, portNumber);
			PrintWriter out =
				new PrintWriter(echoSocket.getOutputStream(), true);
			BufferedReader in =
				new BufferedReader(
					new InputStreamReader(echoSocket.getInputStream()));
			BufferedReader stdIn =
				new BufferedReader(
					new InputStreamReader(System.in))
		){
			
		}catch(IOException i){
			System.err.println(i);
		}
	}
}