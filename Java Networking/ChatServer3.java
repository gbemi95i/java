import java.io.*;
import java.nio.file.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.Scanner;
import java.util.Arrays;
import java.nio.charset.Charset;
public class ChatServer3{
	static boolean expecting=false;
	static String cmd="";
	
	public static void main(String[] args){
		int portNumber=80;
		try( 
			ServerSocket serverSocket = new ServerSocket(portNumber);
			Socket clientSocket = serverSocket.accept();
			DataOutputStream out =
				new DataOutputStream(clientSocket.getOutputStream());
			DataInputStream in =
				new DataInputStream(clientSocket.getInputStream());
		){
			//Thread l=new Thread(new Listener(out,in));
			Thread r=new Thread(new Reader(out,in));
			//l.start();
			r.start();
			
			Scanner sc=new Scanner(System.in);
			Command cm=new Command();
			String outputLine;
			byte[] output;
			
			while((outputLine=sc.nextLine()) != null){
				if(cm.isInstruction(outputLine)){
					output=outputLine.getBytes();
					
					expecting=true;
					cmd=outputLine;
					out.write(output,0,output.length);
				}else{
					output=outputLine.getBytes();
					out.write(output,0,output.length);
					System.out.println("Me(Server):"+outputLine);
				}
				
			}
		}catch(IOException e){
			System.err.println("ERR 1:"+e);
		}
	}
	public static boolean assess(byte[] b, int readLength){
		byte[] actualMessage=new byte[readLength];
		int message;
		if(readLength==1){
			message=b[0];
			if(message==400){
				return false;
			}
		}
		
		return true;
	}
	

	public static void writeToDst(byte[] b, int readLength, String cmd){
		CommandHandler handler = new CommandHandler();
		String[] keys = handler.extractCommandSchema(cmd);
		String to=keys[3];
		//System.out.println("Dst: "+to);
		Path dst=Paths.get(to);
		
		try{
			Files.write(dst, b);
		}catch(IOException e){
			System.err.println("ERR 2:"+e);
		}
	}
	
	class FileSender{
		public byte[] executeInstruction(String inputLine, DataOutputStream out){
			CommandHandler handler = new CommandHandler();
			String[] keys = handler.extractCommandSchema(inputLine);
			
			String action=keys[0];
			String from=keys[1];
			Path file=Paths.get(from);
			byte[] b=null;
			//System.out.println("Got here");
			
			try{
				b=Files.readAllBytes(file);
			}catch(IOException e){}
			
			return b;
		}
	}
	
	
	
	
	
	
	
	static class Reader implements Runnable{
		DataOutputStream out;
		DataInputStream in;

		Reader(DataOutputStream out, DataInputStream in){
			this.out=out;
			this.in=in;
		}
	
		public void run(){
			int readLength;
			String inputLine;
			String outputLine="";
			byte[] b=new byte[2048];
			byte[] output;
			Command cm=new Command();
			ChatServer2.FileSender sender=new ChatServer2().new FileSender();
			
			try{
				while((readLength=in.read(b))!=-1){
					//HANDLING INPUT/REQUEST/RESPONSE
					inputLine=extractString(b, readLength);
					
					if(inputLine.equals("Bye.")){
						break;
					}else if(cm.isInstruction(inputLine)){
						System.out.println("Client:"+inputLine);
						output=sender.executeInstruction(inputLine, out);
						out.write(output,0,output.length);
						
						readLength=in.read(b,0,b.length);
						inputLine=extractString(b, readLength);
						System.out.println("Client:"+inputLine);
					}else if(expecting){
						readLength=b.length;
						if(assess(b,readLength)){
							System.out.println("cmd: "+cmd);
							writeToDst(b,readLength, cmd);
							outputLine="Received. Thanks";
							output=outputLine.getBytes();
							out.write(output,0,output.length);
							System.out.println("Me(Server):"+outputLine);
							
							expecting=false;
						}else{System.out.println("Bad request");};
					}else{
						System.out.println("Client:"+inputLine);
					}
				}
			}catch(IOException e){
				System.err.println("ERR 3:"+e);
			}
		}
		
		
		public static boolean assess(byte[] b, int readLength){
			byte[] actualMessage=new byte[readLength];
			int message;
			if(readLength==1){
				message=b[0];
				if(message==400){
					return false;
				}
			}
			
			return true;
		}
		
		public static String extractString(byte[] b, int readLength){
			byte[] actualMessage=new byte[readLength];
			String message;
			int i=0;
			while(i!=actualMessage.length){
				actualMessage[i]=b[i];
				i++;
			}
			message=new String(actualMessage);
			
			return message;
		}
	}
}