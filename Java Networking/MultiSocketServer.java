import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.Scanner;
public class MultiSocketServer{
	public static void main(String[] args){
		int portNumber=80;
		try( 
			ServerSocket serverSocket = new ServerSocket(portNumber);
		){
			int i=0;
			while(true){
				Socket clientSocket = serverSocket.accept();
				Thread t = new Thread(new Chat(i,clientSocket));
				t.start();
				i++;
			}
		}catch(IOException i){
			System.err.println(i);
		}
	}
	
	static class Chat implements Runnable{
		int id;
		Socket clientSocket;
		Chat(int id, Socket clientSocket){
			this.id=id;
			this.clientSocket=clientSocket;
		}
		
		public void run(){
			try(
				PrintWriter out =
					new PrintWriter(clientSocket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(
					new InputStreamReader(clientSocket.getInputStream()));
			){
				String inputLine, outputLine;
				int i=0;
				while((inputLine=in.readLine())!=null){
					System.out.println("Client:"+inputLine);
					out.println("Received");
					if(inputLine.equals("Bye.")){
						break;
					}
				}
			}catch(IOException i){
				System.err.println(i);
			}
		}
	}
}