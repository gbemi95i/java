import java.io.*;
import java.net.Socket;
import java.util.Scanner;
public class SocketClient{
	public static void main(String[] args){
		//String hostName="192.168.8.100";
		String hostName="localhost";
		int portNumber=80;
		try(
			Socket echoSocket = new Socket(hostName, portNumber);
			PrintWriter out =
				new PrintWriter(echoSocket.getOutputStream(), true);
			BufferedReader in =
				new BufferedReader(
					new InputStreamReader(echoSocket.getInputStream()));
		){
			//System.out.print(echoSocket.isConnected());
			Scanner sc=new Scanner(System.in);
			String userInput;
			while((userInput=sc.nextLine()) != null){//Why this while works? nextLine() blocks tills
				System.out.println("Me:"+userInput);
				out.println(userInput);
				System.out.println("Server:"+in.readLine());
				if(userInput.equalsIgnoreCase("Bye"))
					break;
				//sc=new Scanner(System.in);
			}
		}catch(IOException i){
			System.err.println(i);
		}
	}
}