import java.net.URL;
import java.net.MalformedURLException;
import java.io.*;
public class UrlStream{
	public static void main(String[] args){
		
		try{
			URL link=new URL("http://google.com");
			BufferedReader in=new BufferedReader(new InputStreamReader(link.openStream()));
			//FileReaders doesn't seem to accept url.openStream() as a parameter only InputStreamReader
			
			String inputLine;
			while((inputLine=in.readLine()) != null){
				System.out.println(inputLine);
			}
		}catch(IOException e){
			System.err.println(e);
		}
	}
}