import java.io.*;
import java.nio.file.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.Scanner;
import java.util.Arrays;
import java.nio.charset.Charset;
public class ChatServer2{
	public static void main(String[] args){
		int portNumber=80;
		try( 
			ServerSocket serverSocket = new ServerSocket(portNumber);
			Socket clientSocket = serverSocket.accept();
			DataOutputStream out =
				new DataOutputStream(clientSocket.getOutputStream());
			DataInputStream in =
				new DataInputStream(clientSocket.getInputStream());
		){
			Scanner sc=new Scanner(System.in);
			int readLength;
			String inputLine,outputLine;
			byte[] b=new byte[2048];
			byte[] output;
			Command cm=new Command();
			ChatServer2.FileSender sender=new ChatServer2().new FileSender();
			
			while((readLength=in.read(b))!=-1){//The BufferedReader will keep on reading the input until it reaches the end (end of file or stream or source etc). In this case, the 'end' is the closing of the socket. So as long as the Socket connection is open, your loop will run, and the BufferedReader will just wait for more input, looping each time a '\n' is reached.
				//HANDLING INPUT/REQUEST/RESPONSE

				inputLine=extractString(b, readLength);
				//System.out.println(inputLine);
				
				if(inputLine.equals("Bye.")){
					break;
				}else if(cm.isInstruction(inputLine)){
					System.out.println("Client:"+inputLine);
					output=sender.executeInstruction(inputLine, out);
					out.write(output,0,output.length);
					
					readLength=in.read(b,0,b.length);
					inputLine=extractString(b, readLength);
					System.out.println("Client:"+inputLine);
				}else{
					System.out.println("Client:"+inputLine);
				}
				
				
				//HANDLING OUTPUT/REQUEST/RESPONSE
				//System.out.println("OK");
				outputLine=sc.nextLine();
				//System.out.println(outputLine);
				
				if(cm.isInstruction(outputLine)){
					//System.out.println("Got here 0");
					output=outputLine.getBytes();
					
					out.write(output,0,output.length);
					
					readLength=in.read(b,0,b.length);
					//System.out.println("Got here1");
					if(assess(b,readLength)){
						//System.out.println("Got here2");
						writeToDst(b,readLength, outputLine);
						
						outputLine="Received. Thanks";
						output=outputLine.getBytes();
						out.write(output,0,output.length);
						System.out.println("Me(Server):"+outputLine);
					}else{System.out.println("Bad request");};
				}else{
					output=outputLine.getBytes();
					out.write(output,0,output.length);
					System.out.println("Me(Server):"+outputLine);
				}
				
			}
		}catch(IOException i){
			System.err.println(i);
		}
	}
	
	public static boolean assess(byte[] b, int readLength){
		byte[] actualMessage=new byte[readLength];
		int message;
		if(readLength==1){
			message=b[0];
			if(message==400){
				return false;
			}
		}
		
		return true;
	}
	
	public static void writeToDst(byte[] b, int readLength, String inputLine){
		CommandHandler handler = new CommandHandler();
		String[] keys = handler.extractCommandSchema(inputLine);
		String to=keys[3];
		//System.out.println("Dst: "+to);
		Path dst=Paths.get(to);
		
		try{
			Files.write(dst, b);
		}catch(IOException e){}
	}
			
	public static String extractString(byte[] b, int readLength){
		byte[] actualMessage=new byte[readLength];
		String message;
		int i=0;
		while(i!=actualMessage.length){
			actualMessage[i]=b[i];
			i++;
		}
		message=new String(actualMessage);
		
		return message;
	}
	
	class FileSender{
		public byte[] executeInstruction(String inputLine, DataOutputStream out){
			CommandHandler handler = new CommandHandler();
			String[] keys = handler.extractCommandSchema(inputLine);
			
			String action=keys[0];
			String from=keys[1];
			Path file=Paths.get(from);
			byte[] b=null;
			//System.out.println("Got here");
			
			try{
				b=Files.readAllBytes(file);
			}catch(IOException e){}
			
			return b;
		}
	}
}