import java.io.*;
import java.nio.file.*;
import java.net.Socket;
import java.util.Scanner;
import java.util.Arrays;
public class ChatClient2{
	public static void main(String[] args){
		//String hostName="192.168.8.100";
		String hostName="localhost";
		int portNumber=80;
		try(
			Socket echoSocket = new Socket(hostName, portNumber);
			DataOutputStream out =
				new DataOutputStream(echoSocket.getOutputStream());
			DataInputStream in =
				new DataInputStream(echoSocket.getInputStream());
		){
			//System.out.print(echoSocket.isConnected());
			Scanner sc=new Scanner(System.in);
			String outputLine;
			String inputLine="s";
			int readLength;
			byte[] b=new byte[1024];
			byte[] output;
			Command cm=new Command();
			ChatServer2.FileSender sender=new ChatServer2().new FileSender();
			
			while((outputLine=sc.nextLine()) != null){//Why this while works? nextLine() blocks till
				//HANDLING OUTPUT/REQUEST/RESPONSE
				System.out.println("Me:"+outputLine);
				if(cm.isInstruction(outputLine)){
					output=outputLine.getBytes();
					
					out.write(output,0,output.length);
					
					readLength=in.read(b,0,b.length);
					if(assess(b,readLength)){
						//System.out.println("Got here oo");
						writeToDst(b,readLength, outputLine);
						
						outputLine="Received. Thanks";
						output=outputLine.getBytes();
						out.write(output,0,output.length);
					}else{System.out.println("Bad request");};
				}else{
					output=outputLine.getBytes();
					out.write(output,0,output.length);
				}
				
				
				//HANDLING INPUT/REQUEST/RESPONSE
				readLength=in.read(b);
				inputLine=extractString(b, readLength);
				if(cm.isInstruction(inputLine)){
					output=sender.executeInstruction(inputLine, out);
					out.write(output,0,output.length);
					
					readLength=in.read(b,0,b.length);
					inputLine=extractString(b, readLength);
					System.out.println("Server:"+inputLine);
				}else if(inputLine.equalsIgnoreCase("Bye.")){
					break;
				}else{
					System.out.println("Server:"+inputLine);
				}
				//sc=new Scanner(System.in);*/
			}
		}catch(IOException i){
			System.err.println(i);
		}
	}
	
	public static String extractString(byte[] b, int readLength){
		byte[] actualMessage=new byte[readLength];
		String message;
		int i=0;
		while(i!=actualMessage.length){
			actualMessage[i]=b[i];
			i++;
		}
		message=new String(actualMessage);
		
		return message;
	}

	public static boolean assess(byte[] b, int readLength){
		byte[] actualMessage=new byte[readLength];
		int message;
		if(readLength==1){
			message=b[0];
			if(message==400){
				return false;
			}
		}
		
		return true;
	}
	
	public static void writeToDst(byte[] b, int readLength, String inputLine){
		CommandHandler handler = new CommandHandler();
		String[] keys = handler.extractCommandSchema(inputLine);
		String to=keys[3];
		//System.out.println("Dst: "+to);
		Path dst=Paths.get(to);
		
		try{
			Files.write(dst, b);
		}catch(IOException e){}
	}
}