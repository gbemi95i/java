import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.Scanner;
public class ChatServer{
	public static void main(String[] args){
		int portNumber=80;
		try( 
			ServerSocket serverSocket = new ServerSocket(portNumber);
			Socket clientSocket = serverSocket.accept();
			PrintWriter out =
				new PrintWriter(clientSocket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(
				new InputStreamReader(clientSocket.getInputStream()));
		){
			Scanner sc=new Scanner(System.in);
			String inputLine, outputLine;
			int i=0;
			Command cm=new Command();
			while((inputLine=in.readLine())!=null){//The BufferedReader will keep on reading the input until it reaches the end (end of file or stream or source etc). In this case, the 'end' is the closing of the socket. So as long as the Socket connection is open, your loop will run, and the BufferedReader will just wait for more input, looping each time a '\n' is reached.
				System.out.println("Client:"+inputLine);
				if(inputLine.equals("Bye.")){
					break;
				}else if(cm.isInstruction(inputLine)){
					System.out.println("yes");
				}else{
					out.println("Received");
					outputLine=sc.nextLine();
					out.println(outputLine);
				}
			}
		}catch(IOException i){
			System.err.println(i);
		}
	}
}