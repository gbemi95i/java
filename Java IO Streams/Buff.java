import java.io.*;
public class Buff{
	public static void main(String[] args){
		try(
			BufferedInputStream bis=new BufferedInputStream(new FileInputStream("MyNums.txt"));
			BufferedOutputStream bos=new BufferedOutputStream(new FileOutputStream("MyNums2.txt"));
		){
			byte[] b=new byte[1024];
			int l=0;
			while((l=bis.read(b))>=0){
				bos.write(b);
			}
		}catch(IOException e){}
	}
}