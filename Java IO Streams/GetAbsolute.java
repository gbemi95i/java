import java.io.*;
import java.io.IOException;
import java.nio.file.*;
import static java.nio.file.StandardCopyOption.*;
import java.net.URI;
public class GetAbsolute{
	public static void main(String[] args){
		Path f = Paths.get("Sample/random1.java");
		System.out.println(f.toAbsolutePath());
		System.out.println(f.toAbsolutePath().getParent());
	}
}