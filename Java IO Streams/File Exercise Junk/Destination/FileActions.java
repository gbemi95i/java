import java.io.IOException;
import java.nio.file.*;
import static java.nio.file.StandardCopyOption.*;
public interface FileActions{
	public void copyFile(Path src,Path dst);

	public void moveFile(File src,File dst);

	public void renameFile(File src, String name);
	
	public boolean checkFile(File file);
	
	boolean containsString(File file, String sampleString);
	
	int firstOccurenceInFile(File file, String sampleString);
	
	int lastOccurentInFile(File file, String sampleString);
	
	void deleteFile(File file);
	
	File[] findFilesWithString(File directory, String sampleString);
}