import java.io.IOException;
import java.nio.file.*;
import static java.nio.file.StandardCopyOption.*;
public class FileAction{
	public void copyFile(Path src,Path dst){
		if(checkFile(src)){
			try{
				Files.copy(src,dst,REPLACE_EXISTING);
				System.out.println(src.getFileName()+" copied to "+dst);
			}catch(IOException e){
				System.err.println("IO Error: "+e);
			}
		}else{
			System.err.println("File Error: File/file directory is invalid! "+src);
		}
	}

	public void moveFile(Path src,Path dst){
		if(checkFile(src)){
			try{
				Files.move(src,dst,REPLACE_EXISTING);
				System.out.println(src.getFileName()+" moved to "+dst);
			}catch(IOException e){
				System.err.println("IO Error: "+e);
			}
		}else{
			System.err.println("File Error: File/file directory is invalid! "+src);
		}
	}

	public void renameFile(Path src, String name){
		if(checkFile(src)){
			String p=src.toAbsolutePath().getParent()+"/"+name;
			Path newPath=Paths.get(p);
			try{
				Files.move(src,newPath);
				System.out.println(src.getFileName()+" renamed to "+name);
			}catch(IOException e){
				System.err.println("IO Error: "+e);
			}
		}else{
			System.err.println("File Error: File/file directory is invalid! "+src);
		}
	}
	
	public boolean checkFile(Path file){
		boolean isRegularExecutableFile = Files.isRegularFile(file) && !Files.isDirectory(file, LinkOption.NOFOLLOW_LINKS);
		
		/** &
         Files.isReadable(file) & Files.isExecutable(file);**/
		 
		return isRegularExecutableFile;
	}
}