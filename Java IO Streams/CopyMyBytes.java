import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyMyBytes{
	public static void main(String[] args) throws IOException{
		FileInputStream in=null;
		FileOutputStream out=null;
		
		try{
			in=new FileInputStream("My Alphabets.txt");
			out=new FileOutputStream("My Second Alphabets.txt");
			int c;
			
			while((c=in.read()) != -1){/**THE READ METHOD READS THE NEXT BYTE OF DATA
				FROM THE INPUT STREAM AND RETURNS AN INT BETWEEN 0 TO 255 UNTIL THE
				END OF THE STREAM WHERE IT WOULD RETURN -1.**/
				/**char p = (char)c;
				System.out.println(p);**/
				
				out.write(c);/**THE WRITE METHOD TAKES THE INT AND WRITES THE BYTE
					VALUE TO THE OUTPUT STREAM**/
			}
		}finally{
			if(in != null){
				in.close();
			}
			if(out != null){
				out.close();
			}
		}
	}
}