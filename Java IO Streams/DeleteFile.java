import java.io.*;
import java.io.IOException;
import java.nio.file.*;
import java.net.URI;
public class DeleteFile{
	public static void main(String[] args){
		Path p1 = Paths.get("C:\\Java\\Java IO Streams\\Sample\\random1.java");
		Path p2 = Paths.get(".\\Sample\\random2.java");
		Path p3 = Paths.get(".\\Sample");
		System.out.println(p1.getRoot());
		try{
			Files.delete(p1);
			Files.delete(p2);
			Files.delete(p3);
		}/**catch(NoSuchFileException x){
			System.err.println(x);
		}catch(DirectoryNotEmptyException d){
			System.out.println(d);
		}**/catch(IOException e){
			System.err.println("Okurrr: "+e);
		}
	}
}