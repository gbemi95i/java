import java.io.*;
import java.io.IOException;
import java.nio.file.*;
import static java.nio.file.StandardCopyOption.*;
import java.net.URI;
public class FindFiles{
	public static void main(String[] args){
		Path f = Paths.get("./Sample/random1.java");
		Path f2 = Paths.get("./Sample/random1.html");
		PathMatcher finder=FileSystems.getDefault().getPathMatcher("glob:"+"*.java");
		if(finder.matches(f.getFileName())){
				System.out.println(f);
		}
		if(finder.matches(f2.getFileName())){
				System.out.println(f2);
		}
	}
}