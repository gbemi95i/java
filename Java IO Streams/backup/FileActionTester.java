import java.io.*;
public class FileActionTester{
	
	//TEST COPIER
	public boolean testCopier(File src, File dst){
		
		//File src;
		//File dst;
		File backup;
		
		
		boolean verify=true;
		
		//src=new File("src/My Numbers.txt");
		//dst=new File("Destination/My Numbers.txt");
		backup=new File("../backup");
		resetEnvironment(src,dst,backup);
		//copy(src,dst);
		//if(!checkCopiedFile(src,dst)){verify=false;};
		
		/**src=new File("My Numbers2.txt");
		dst=new File("Destination/My Numbers2.txt");
		copy(src,dst);
		if(!checkCopiedFile(src,dst)){verify=false;};
		
		src=new File("My Numbers3.txt");
		dst=new File("Destination/My Numbers3.txt");
		copy(src,dst);
		if(!checkCopiedFile(src,dst)){verify=false;};
		
		src=new File("My Numbers4.txt");
		dst=new File("Destination/My Numbers4.txt");
		copy(src,dst);
		if(!checkCopiedFile(src,dst)){verify=false;};**/
		
		
		return verify;
	}
	

	
	public static boolean checkCopiedFile(File src, File dst){
		if(src.length()!=dst.length()){
			return false;
		}else{
			//System.out.println(src.length());
			return true;
		}
	}
	
	public static void copy(File src, File dst){
		FileAction file=new FileAction();
		file.copyFile(src,dst);
	}
	
	
	
	
	
	
	
	//TEST MOVER
	public boolean testMover(){
		//resetEnvironment();
		
		File src;
		File dst;
		File backup;
		
		boolean verify=true;
		
		src=new File("src/My Alphabets.txt");
		dst=new File("Destination/My Alphabets.txt");
		backup=new File("./backup");
		resetEnvironment(src,dst,backup);
		move(src,dst);
		if(!checkMovedFile(src,dst)){verify=false;};
		
		/**src=new File("My Alphabets2.txt");
		dst=new File("Destination/My Alphabets2.txt");
		move(src,dst);
		if(!checkMovedFile(src,dst)){verify=false;};
		
		src=new File("My Alphabets3.txt");
		dst=new File("Destination/My Alphabets3.txt");
		move(src,dst);
		if(!checkMovedFile(src,dst)){verify=false;};
		
		src=new File("My Alphabets4.txt");
		dst=new File("Destination/My Alphabets4.txt");
		move(src,dst);
		if(!checkMovedFile(src,dst)){verify=false;};**/
		
		
		return verify;
	}
	
	public static boolean checkMovedFile(File src, File dst){
		if((!dst.exists()) || src.exists()){
			return false;
		}else{
			//System.out.println(src.length());
			return true;
		}
	}
	
	public static void move(File src, File dst){
		FileAction file=new FileAction();
		file.moveFile(src,dst);
	}
	
	
	
	
	
	
	
	
	//TEST RENAMER
	public boolean testRenamer(){
		File src;
		File dst;
		File backup;
		String newName;
		boolean verify=true;
		
		src=new File("src/A.txt");
		dst=new File("Destination/1.txt");
		newName="src/1.txt";
		backup=new File("./backup");
		resetEnvironment(src,dst,backup);
		rename(src,newName);
		if(!checkRenamedFile(src,newName)){verify=false;};
		
		/**file=new File("src/B.txt");
		newName="2.txt";
		rename(file,newName);
		if(!checkRenamedFile(file,newName)){verify=false;};
		
		file=new File("src/C.txt");
		newName="3.txt";
		rename(file,newName);
		if(!checkRenamedFile(file,newName)){verify=false;};
		
		file=new File("src/D.txt");
		newName="4.txt";
		rename(file,newName);
		if(!checkRenamedFile(file,newName)){verify=false;};**/
		
		
		return verify;
	}
	
	public static boolean checkRenamedFile(File file, String newName){
		if(file.getName().equals(newName)){
			return false;
		}else{
			// System.out.println(file.getName());
			// System.out.println(newName);
			return true;
		}
	}
	
	public static void rename(File file, String newName){
		FileAction fAction=new FileAction();
		fAction.renameFile(file, newName);
	}
	
	
	
	
	
	

	//CONTAINS STRING
	public static boolean containsString(){
		File file=new File("src/alphabets.txt");
		String str="de";
		
		
		FileAction fAction=new FileAction();
		
		boolean result=fAction.containsString(file, str);
		System.out.println(result);
		return result;
	}

	//FIRST OCCURENCE IN FILE
	public static int firstOccurenceInFile(){
		File file=new File("src/alphabets.txt");
		String str="de";
		
		
		FileAction fAction=new FileAction();
		int result=fAction.firstOccurenceInFile(file, str);
		System.out.println(result);
		return result;
	}
	
	//LAST OCCURENCE IN FILE
	public static int lastOccurenceInFile(){
		File file=new File("src/alphabets.txt");
		String str="de";
		
		
		FileAction fAction=new FileAction();
		int result=fAction.lastOccurentInFile(file, str);
		System.out.println(result);
		return result;
	}
	
	//FIND FILES WITH STRING
	public static File[] findFilesWithString(){
		File file=new File("C:/Java/Java IO Streams/new/dest2");
		String str="z";
		
		
		FileAction fAction=new FileAction();
		
		return fAction.findFilesWithString(file, str);
	}
	
	
	
	
	
	
	//THE RESETERS
	public static void resetEnvironment(File src, File dst, File backup){
		String str=dst.getAbsoluteFile().getParent();
		System.out.println("dstParent"+str);
		//emptyDir(str);
		
		
		str=src.getAbsoluteFile().getParent();
		System.out.println("srcParent"+str);
		//emptyDir(str);
		//System.out.println(backup.getAbsoluteFile());
		
		
		transfer(backup,src);
		
		
		//EMPTYING BACKUP DIR AFTER BACKUP
		str=backup.getAbsolutePath();
		System.out.println("backup: "+str);
		//emptyDir(str);
		
		src=new File(src.getAbsoluteFile().getParent());
		System.out.println("srcParent"+str);
		//transfer(src,backup);
	};

	public static void emptyDir(String str){
		File dst=new File(str);
		//System.out.println(str);

		File[] files = dst.listFiles();
		
		for(File x: files){
			if(x.isDirectory()){
				//System.out.println(x.listFiles().length);
				if(x.listFiles().length!=0){
					String str2=x.getAbsolutePath();
					emptyDir(str2);
					x.delete();
				}else{
					//System.out.println(x.getName());
					x.delete();
				}
			}else{
				x.delete();
			}
			//System.out.println(x.getName());
		}
		
		//dst.delete();
	};
	
	public static void transfer(File f1, File f2){
		File[] files = f1.listFiles();
		
		for(File x: files){
			if(x.isDirectory()){
				String str;
				if(!f2.isDirectory()){
					str=f2.getAbsoluteFile().getParent()+"/"+x.getName();
					//System.out.println(str);
					File f3=new File(str);
					f3.mkdir();
					transfer(x,f3);
				}else{
					str=f2.getAbsoluteFile()+"/"+x.getName();
					//System.out.println(str);
					File f3=new File(str);
					f3.mkdir();
					transfer(x,f3);
				}
			}else{
				if(!f2.isDirectory()){
					String str=f2.getAbsoluteFile().getParent()+"/"+x.getName();
					File y=new File(str);
					copy(x,y);
				}else{
					String str=f2.getAbsoluteFile()+"/"+x.getName();
					File y=new File(str);
					copy(x,y);					
				}
			}
			//System.out.println(x.getName());
		}
	}
}