import java.io.*;
public class TestMover{
	public boolean test(){
		return testMover();
	}
	
	public boolean testMover(){
		File src;
		File dst;
		
		boolean verify=true;
		
		src=new File("My Alphabets.txt");
		dst=new File("Destination/My Alphabets.txt");
		move(src,dst);
		if(!checkMovedFile(src,dst)){verify=false;};
		
		src=new File("My Alphabets2.txt");
		dst=new File("Destination/My Alphabets2.txt");
		move(src,dst);
		if(!checkMovedFile(src,dst)){verify=false;};
		
		src=new File("My Alphabets3.txt");
		dst=new File("Destination/My Alphabets3.txt");
		move(src,dst);
		if(!checkMovedFile(src,dst)){verify=false;};
		
		src=new File("My Alphabets4.txt");
		dst=new File("Destination/My Alphabets4.txt");
		move(src,dst);
		if(!checkMovedFile(src,dst)){verify=false;};
		
		
		return verify;
	}
	
	public static boolean checkMovedFile(File src, File dst){
		//File old=new File(src.getAbsolutePath());
		if((!dst.exists()) || src.exists()){
			System.out.println(src.getAbsolutePath());
			return false;
		}else{
			//System.out.println(src.length());
			return true;
		}
	}
	
	public static void move(File src, File dst){
		
		
		FileAction file=new FileAction();
		file.moveFile(src,dst);
	}
}