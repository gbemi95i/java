import java.util.Scanner;
public class CommandHandler{
	public static void main(String[] args){

	}
	
	public static String[] extractCommandSchema(String txt){
		/**String txt1="Copy MyNumber.txt to MyNumber2.txt";
		String txt2="Copy MyNumber.txt to \"My Number2.txt\"";
		String txt3="Copy \"My Number.txt\" to MyNumber2.txt";
		String txt4="Copy \"My Number.txt\" to \"My Number2.txt\"";**/
		
		//String txt=txt4;
		String[] keys=null;
		
		if(getQuotesNum(txt)==0){
			Scanner s=new Scanner(txt);
			String action=s.next();
			String from=s.next();
			String loc=s.next();
			String to=s.next();
			keys=new String[]{action,from,loc,to};
			//System.out.println(action+"."+from+"."+loc+"."+to);
			//System.out.println(keys.length);
		}else if(getQuotesNum(txt)!=4 && getQuotesNum(txt)!=2){
			//System.out.println("Nah");
			keys=new String[1];
		}else{
			String[] arr = getSplitParts(txt);
			if(arr.length==4){
				String action=arr[0];
				String from=arr[1];
				String loc=arr[2];
				String to=arr[3];
				keys=new String[]{action,from,loc,to};
				//System.out.println(action+"."+from+"."+loc+"."+to);
				//System.out.println(keys.length);
			}else if(arr.length==3){
				String action=arr[0];
				String from=arr[1];
				Scanner s = new Scanner(arr[2]);
				String loc=s.next();
				String to=s.next();
				keys=new String[]{action,from,loc,to};
				//System.out.println(action+"."+from+"."+loc+"."+to);
				//System.out.println(keys.length);
			}else if(arr.length==2){
				Scanner s = new Scanner(arr[0]);
				String action=s.next();
				String from=s.next();
				String loc=s.next();
				String to=arr[1];
				keys=new String[]{action,from,loc,to};
				//System.out.println(action+"."+from+"."+loc+"."+to);
				//System.out.println(keys.length);
			}else if(arr.length==1){
				Scanner s = new Scanner(arr[0]);
				String action=s.next();
				String from=s.next();
				String loc=s.next();
				String to=arr[1];
				keys=new String[]{action,from,loc,to};
				//System.out.println(action+"."+from+"."+loc+"."+to);
				//System.out.println(keys.length);
			}
		}
		
		
		return keys;
	}
	
	public static String[] getSplitParts(String txt){
		//System.out.println(txt);
		
		String[] arr=txt.split("\"");
		for(int i=0; i<arr.length; i++){
			//String x=arr[i].trim();
			//System.out.println(x);
			arr[i]=arr[i].trim();
		}
		
		return arr;
	}
	
	/**public static void getKeys(String cmd){
		//String txt1="Copy My Number.txt to My Number2.txt";
		//String txt2="Copy \"My Number.txt\" to \"My Number2.txt\"";
		//String txt3="Copy \"My Number.txt\" to My Number2.txt";
		String txt4="Copy \"My Number.txt\" to MyNumber2.txt";
		//System.out.println(txt1);
		//System.out.println(txt2);
		//System.out.println(txt3);
		System.out.println(txt4);
		
		//String[] arr=txt2.split("\"");
		//String[] arr=txt3.split("\"");
		
		String[] arr=txt4.split("\"");
		for(int i=0; i<arr.length; i++){
			if(i==2){
				String x=arr[i].trim();
				System.out.println(x);
			}
		}
	}**/
	
	public static int getQuotesNum(String txt){
		//System.out.println(txt);
		String[] arr=txt.split("");
		int j=0;
		for(int i=0; i<arr.length; i++){
			if(arr[i].equals("\""))
				j++;
		}
		//System.out.println(j);
		return j;
	}
	
}