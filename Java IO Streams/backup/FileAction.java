import java.io.*;
import java.util.ArrayList;
public class FileAction implements FileActionsInter{
	
	public void copyFile(File src,File dst){
		
		//COPY FILE
		try(FileInputStream in=new FileInputStream(src);
			FileOutputStream out=new FileOutputStream(dst)){
			int c;
			
			while((c=in.read()) != -1){/**THE READ METHOD READS THE NEXT BYTE OF DATA
				FROM THE INPUT STREAM AND RETURNS AN INT BETWEEN 0 TO 255 UNTIL THE
				END OF THE STREAM WHERE IT WOULD RETURN -1.**/

				out.write(c);/**THE WRITE METHOD TAKES THE INT AND WRITES THE BYTE
					VALUE TO THE OUTPUT STREAM**/
			}
		}catch(IOException e){
			System.err.println(e);
		}//YOU RUN THE RISK OF OPEN STREAMS (CATCH VS THROWS IN METHODS SIGNATURE)
	}

	public void moveFile(File src,File dst){
		//FileInputStream in=null;
		//FileOutputStream out=null;
		
		//COPY FILE & DELETE OLD
		try(FileInputStream in=new FileInputStream(src);
			FileOutputStream out=new FileOutputStream(dst)){
			int c;
			
			while((c=in.read()) != -1){

				out.write(c);
			}
		}catch(IOException e){
			System.err.println(e);
		}
			src.delete();
	}
	
	public void renameFile(File file, String newName){
		File newRenamedFile=new File(newName);
		//RENAME FILE
		file.renameTo(newRenamedFile);
	}
	
	public void validFile(File src, String name){
		/**if(checkFile(src)){
			String p=src.toAbsoluteFile().getParent()+"/"+name;
			File newFile=Files.get(p);
			try{
				Files.move(src,newFile);
				System.out.println(src.getFileName()+" renamed to "+name);
			}catch(IOException e){
				System.err.println("IO Error: "+e);
			}
		}else{
			System.err.println("File Error: File/file directory is invalid! "+src);
		}**/
	}


	public boolean checkFile(File file){return true;};
	
	public boolean containsString(File file, String sampleString){
		String content="";
		
		//EXTRACT FILE CONTENT
		try(FileInputStream in=new FileInputStream(file)){
			int b;
			int i=0;
			while((b=in.read()) != -1){
				char c = (char) b;
				content=content+c;
				/**if(i==7){
					if(c=='\r'){
						System.out.println("Yes");
					}else{
						System.out.println("Wasting time");
					}
				}
				i++;**/
			}
		}catch(IOException e){
			System.err.println(e);
		}
		
		//System.out.println(content);
		//boolean result=isFound(content,sampleString);
		
		//CHECK IF CONTENT CONTAINS STRING
		return content.contains(sampleString);
	};
	
	public int firstOccurenceInFile(File file, String sampleString){
		String content="";
		try(FileInputStream in=new FileInputStream(file)){
			int b;
			int i=0;
			while((b=in.read()) != -1){
				char c = (char) b;
				content=content+c;
			}
		}catch(IOException e){
			System.err.println(e);
		}
		
		System.out.println(content);

		//CHECK IF CONTENT CONTAINS STRING
		if(!content.contains(sampleString)){
			return -1;
		}else{
			//System.out.println(content.indexOf(sampleString));
			//CHECK STRING FIRST INDEX
			return content.indexOf(sampleString);
		}
	};
	
	public int lastOccurentInFile(File file, String sampleString){
		String content="";

		//EXTRACT FILE CONTENT
		try(FileInputStream in=new FileInputStream(file)){
			int b;
			int i=0;
			while((b=in.read()) != -1){
				char c = (char) b;
				content=content+c;
			}
		}catch(IOException e){
			System.err.println(e);
		}
		
		System.out.println(content);

		//CHECK IF CONTENT CONTAINS STRING
		if(!content.contains(sampleString)){
			return -1;
		}else{
			//System.out.println(content.lastIndexOf(sampleString));
			//CHECK STRING FIRST INDEX
			return content.lastIndexOf(sampleString);
		}
	};
	
	public void deleteFile(File file){
		file.delete();
	};
	
	public File[] findFilesWithString(File directory, String sampleString){
		ArrayList<File> fileList = new ArrayList<File>();
		for(File x : directory.listFiles()){
			if(containsString(x, sampleString)){
				fileList.add(x);
			}
		}
		
		int len=fileList.size();
		//System.out.println(len);
		File[] files=new File[len];
		files=fileList.toArray(new File[len]);
		//System.out.println(files.length);
		// System.out.println(listOfFiles.length);
		// System.out.println(directory.getName());
		
		return files;
	};
	
	/**public boolean isFound(String content, String sampleString){
		
	}
	
	public boolean scanThrough(content, , ){
		
	}**/
}