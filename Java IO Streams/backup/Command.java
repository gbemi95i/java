import java.io.*;
import java.util.Scanner;
public class Command{
    
    public static void main (String args[]){

        Console c = System.console();
        if (c == null) {
            System.err.println("No console.");
            System.exit(1);
        }

        String cmd = c.readLine("Input your command: ");
		cmd=cmd.toLowerCase();
        //checkCommandValidity(cmd);
        if(checkCommandValidity(cmd)){
			System.out.println("Valid");

			CommandHandler handler = new CommandHandler();
			String[] keys = handler.extractCommandSchema(cmd);
			
			int l=keys.length;
			if(l==4){
				String action=keys[0];
				String from=keys[1];
				String loc=keys[2];
				String to=keys[3];
				
				File src=new File(from);
				File dst=new File(to);
				FileActionTester tester=new FileActionTester();
				boolean result=tester.testCopier(src,dst);
				System.out.println(result);
			}else{
				
			}
			
		}else{
			//System.out.println("Please check your command properly.");
		};
		//System.out.println(cmd);
		/**Scanner s = new Scanner(cmd);
		while(s.hasNext()){
			String x=(String) s.next();
			System.out.println(x);
		}**/
    }
	
	public static void getType(String cmd){
		Scanner s=new Scanner(cmd);
		String c;
		c=(String) s.next();
		String type=null;
		if(c.equals("copy")){
			//type="copy";
			executeCopy(cmd);
		}else if(c.equals("move")){
			type="move";
		}else if(c.equals("rename")){
			type="rename";
		}else if(c.equals("find")){
			type="find";
		}else if(c.equals("firstOccurence")){
			type="firstOccurence";
		}else if(c.equals("lastOccurence")){
			type="lastOccurence";
		}else if(c.equals("delete")){
			type="delete";
		}
		//return type;
	}
	
	public static void executeCopy(String cmd){
		Scanner s=new Scanner(cmd);
		String c;
		c=(String) s.next();
		File from = new File(c);
		c=(String) s.next();//this gives "to"
		c=(String) s.next();//this is the destination file
		File to = new File(c);
		
		FileActionTester tester=new FileActionTester();
		boolean result=tester.testCopier(from,to);
	}
	
	public static boolean checkCommandValidity(String cmd){
		boolean valid;
		
		CommandHandler handler = new CommandHandler();
		String[] keys = handler.extractCommandSchema(cmd);
		int l=keys.length;
		if(l==4){
			String action=keys[0];
			String from=keys[1];
			String loc=keys[2];
			String to=keys[3];
			
			File f;
			
			if(action.equals("copy") || action.equals("move") || action.equals("rename") || action.equals("find") || action.equals("firstOccurence") || action.equals("lastOccurence")){
				f=new File(from);
				
				if(!f.isFile() && !f.exists()){
					valid=false;
					System.out.println("Please check your command properly.");
				}else{
					if(!loc.equals("in") && !loc.equals("to")){
						valid=false;
						System.out.println("Please check your command properly.");
					}else{
						valid=true;
					}
				}
				valid=true;
			}else{
				valid=false;
				System.out.println("Please check your command properly.");
			}
		}else if(l==2){
			String action=keys[0];
			String file=keys[1];
			File f;
			
			f=new File(file);
			if(!f.isDirectory() && !f.isFile() && !f.exists()){
				System.out.println("Please check your command properly.");
				valid=false;
			}else{
				valid=true;
			}
			
		}else{
			valid=false;
			System.out.println("Please check your command properly.");
		}
		
		return valid;
	}

	public static void retrieveName(String cmd){
		
	}
}