import java.io.*;
public class TestCopier{
	public boolean test(){
		return testCopier();
	}
	
	public boolean testCopier(){
		File src;
		File dst;
		
		boolean verify=true;
		
		src=new File("My Numbers.txt");
		dst=new File("Destination/My Numbers.txt");
		copy(src,dst);
		if(!checkCopiedFile(src,dst)){verify=false;};
		
		src=new File("My Numbers2.txt");
		dst=new File("Destination/My Numbers2.txt");
		copy(src,dst);
		if(!checkCopiedFile(src,dst)){verify=false;};
		
		src=new File("My Numbers3.txt");
		dst=new File("Destination/My Numbers3.txt");
		copy(src,dst);
		if(!checkCopiedFile(src,dst)){verify=false;};
		
		src=new File("My Numbers4.txt");
		dst=new File("Destination/My Numbers4.txt");
		copy(src,dst);
		if(!checkCopiedFile(src,dst)){verify=false;};
		
		
		return verify;
	}
	
	public static boolean checkCopiedFile(File src, File dst){
		if(src.length()!=dst.length()){
			return false;
		}else{
			//System.out.println(src.length());
			return true;
		}
	}
	
	public static void copy(File src, File dst){
		FileAction file=new FileAction();
		file.copyFile(src,dst);
	}
}