import java.io.*;
public class TestRenamer{
	public boolean test(){
		return testRenamer();
	}
	
	public boolean testRenamer(){
		File file;
		String newName;
		
		boolean verify=true;
		
		file=new File("A.txt");
		newName="1.txt";
		rename(file,newName);
		if(!checkRenamedFile(file,newName)){verify=false;};
		
		file=new File("B.txt");
		newName="2.txt";
		rename(file,newName);
		if(!checkRenamedFile(file,newName)){verify=false;};
		
		file=new File("C.txt");
		newName="3.txt";
		rename(file,newName);
		if(!checkRenamedFile(file,newName)){verify=false;};
		
		file=new File("D.txt");
		newName="4.txt";
		rename(file,newName);
		if(!checkRenamedFile(file,newName)){verify=false;};
		
		
		return verify;
	}
	
	public static boolean checkRenamedFile(File file, String newName){
		if(file.getName().equals(newName)){
			return false;
		}else{
			// System.out.println(file.getName());
			// System.out.println(newName);
			return true;
		}
	}
	
	public static void rename(File file, String newName){
		FileAction fAction=new FileAction();
		fAction.renameFile(file, newName);
	}
}