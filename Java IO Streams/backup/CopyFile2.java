import java.io.*;
import java.io.IOException;
import java.nio.file.*;
import static java.nio.file.StandardCopyOption.*;
import java.net.URI;
public class CopyFile2{
	
	public static void main(String[] args){
		File f1 = new File("./dest2");
		File f2 = new File("./dest3");
		copyDir(f1,f2);
	}
	
	public static void copyDir(File f1, File f2){
		/**if(!f2.exists()){
			f2.mkdir();
		}**/
		
		copyDirFiles(f1,f2);
	}
	
	public static void copyDirFiles(File f1, File f2){
		File[] files = f1.listFiles();
		
		for(File x: files){
			if(x.isDirectory()){
				String str=f2.getAbsolutePath()+"/"+x.getName();
				File f3=new File(str);
				f3.mkdir();
				copyDirFiles(x,f3);
			}else{
				String str=f2.getAbsoluteFile()+"/"+x.getName();
				File y=new File(str);
				copy(x,y);
			}
			//System.out.println(x.getName());
		}
	}
	
	public static void copy(File src, File dst){
		FileAction file=new FileAction();
		file.copyFile(src,dst);
	}
}