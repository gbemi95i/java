import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CopyMyChars{
	public static void main(String[] args) throws IOException{
		FileReader in=null;
		FileWriter out=null;
		int c;
		
		try{
			in=new FileReader("My Numbers.txt");
			out=new FileWriter("My Second Numbers.txt");
			
			while((c=in.read()) != -1){/**THE READ METHOD READS THE NEXT BYTE OF DATA
					FROM THE INPUT STREAM AND RETURNS AN INT BETWEEN 0 TO 255 UNTIL THE
					END OF THE STREAM WHERE IT WOULD RETURN -1.**/
				
				out.write(c);/**THE WRITE METHOD TAKES THE INT AND WRITES THE BYTE
					VALUE TO THE OUTPUT STREAM**/
			}
		}finally{
			if(in != null){
				in.close();
			}
			if(out != null){
				out.close();
			}
		}
		
	}
}