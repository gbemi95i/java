import java.io.*;
import java.util.Scanner;

public class ScanMyBytes{
	public static void main(String[] args) throws IOException{
		//BufferStream in=null;
		FileOutputStream out=null;
		Scanner s=null;
		
		try{
			s = new Scanner(new BufferedInputStream(new FileInputStream("My Alphabets3.txt")));
			out= new FileOutputStream("My Alphabets4.txt");
			
			while(s.hasNext()){
				//out.write(s.nextInt());
				System.out.println(s.next());
			}
		}finally{
			if(s != null){
				s.close();
			}
			if(out != null){
				out.close();
			}
		}
	}
}