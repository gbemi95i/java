import java.io.*;
public class FileActionTest{
	
	static FileActionsInter fileAction;
	
	public static void main(String[] args){
		FileActionTest faTester = new FileActionTest();
		
		resetEnvironment();
		
		faTester.fileAction = new NewIOFileAction();
		faTester.runTests();
		
		resetEnvironment();
		
		faTester.fileAction = new FileAction(); //NewIOFileAction implements FileActionsInter but does things with in the NIO way.
		faTester.runTests();
	}
	
	/**private static void resetTestDirectory(){
		//Do reset
	}**/
	
	private static void runTests(){
		long start = System.currentTimeMillis();
		System.out.println("Actual copy started:");
		System.out.println(testMover());
		long end = System.currentTimeMillis();
		long time=end-start;
		
		System.out.println((time)+" milliseconds");
		System.out.println((time/1000)+" seconds");
		//System.out.println(testMover());
		//System.out.println(testRenamer());
		//testDelete();
		//etc
		
		//System.out.println(containsString());
	}
	
	
	
	
	
	
	//... the test methods ...
	//TEST COPIER
	public static boolean testCopy(){
		boolean verify=true;
		
		File src=new File("src/IA.mp3");
		File dst=new File("dst/IA.mp3");
		fileAction.copyFile(src,dst);
		if(!checkCopiedFile(src,dst)){verify=false;};
		
		
		return verify;
	}
	public static boolean checkCopiedFile(File src, File dst){
		if(src.length()!=dst.length()){
			return false;
		}else{
			//System.out.println(src.length());
			return true;
		}
	}
	
	
	
	
	
	
	
	//TEST MOVER
	public static boolean testMover(){
		boolean verify=true;
		
		File src=new File("src/IA.mp3");
		File dst=new File("dst/IA.mp3");
		fileAction.moveFile(src,dst);
		if(!checkMovedFile(src,dst)){verify=false;};
		
		
		return verify;
	}
	
	public static boolean checkMovedFile(File src, File dst){
		if((!dst.exists()) || src.exists()){
			return false;
		}else{
			//System.out.println(src.length());
			return true;
		}
	}
	
	
	
	
	
	
	//TEST RENAMER
	public static boolean testRenamer(){
		String newName="src/1.txt";
		boolean verify=true;
		File src=new File("src/A.txt");
		
		fileAction.renameFile(src,newName);
		if(!checkRenamedFile(src,newName)){verify=false;};
		
		
		return verify;
	}
	
	public static boolean checkRenamedFile(File file, String newName){
		if(file.getName().equals(newName)){
			return false;
		}else{
			// System.out.println(file.getName());
			// System.out.println(newName);
			return true;
		}
	}
	
	
	
	
	public static boolean containsString(){
		File file=new File("src/My Alphabets.txt");
		String str="13";
		
		boolean result=fileAction.containsString(file, str);
		//System.out.println(result);
		return result;
	}
	
	
	
	
	
	//THE RESETERS
	public static void resetEnvironment(){
		File src=new File("src/My Number.txt");
		File dst=new File("dst/My Number.txt");
		File backup=new File("./backup");
		
		String str=dst.getAbsoluteFile().getParent();
		//System.out.println("dstParent"+str);
		emptyDir(str);
		
		
		str=src.getAbsoluteFile().getParent();
		//System.out.println("srcParent"+str);
		emptyDir(str);
		//System.out.println(backup.getAbsoluteFile());
		
		
		transfer(backup,src);
		
		
		//EMPTYING BACKUP DIR BEFORE BACKUP
		str=backup.getAbsolutePath();
		//System.out.println("backup: "+str);
		emptyDir(str);
		
		src=new File(src.getAbsoluteFile().getParent());
		//System.out.println("srcParent"+str);
		transfer(src,backup);
	};

	public static void emptyDir(String str){
		File dst=new File(str);
		//System.out.println(str);

		File[] files = dst.listFiles();
		
		for(File x: files){
			if(x.isDirectory()){
				//System.out.println(x.listFiles().length);
				if(x.listFiles().length!=0){
					String str2=x.getAbsolutePath();
					emptyDir(str2);
					x.delete();
				}else{
					//System.out.println(x.getName());
					x.delete();
				}
			}else{
				x.delete();
			}
			//System.out.println(x.getName());
		}
		
		//dst.delete();
	};
	
	public static void transfer(File f1, File f2){
		File[] files = f1.listFiles();
		
		for(File x: files){
			if(x.isDirectory()){
				String str;
				if(!f2.isDirectory()){
					str=f2.getAbsoluteFile().getParent()+"/"+x.getName();
					//System.out.println(str);
					File f3=new File(str);
					f3.mkdir();
					transfer(x,f3);
				}else{
					str=f2.getAbsoluteFile()+"/"+x.getName();
					//System.out.println(str);
					File f3=new File(str);
					f3.mkdir();
					transfer(x,f3);
				}
			}else{
				if(!f2.isDirectory()){
					String str=f2.getAbsoluteFile().getParent()+"/"+x.getName();
					File y=new File(str);
					copy(x,y);
				}else{
					String str=f2.getAbsoluteFile()+"/"+x.getName();
					File y=new File(str);
					copy(x,y);					
				}
			}
			//System.out.println(x.getName());
		}
	}
	
	public static void copy(File src, File dst){
		NewIOFileAction file=new NewIOFileAction();
		file.copyFile(src,dst);
	}
}