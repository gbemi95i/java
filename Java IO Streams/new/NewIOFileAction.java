import java.io.*;
import java.nio.file.*;
import static java.nio.file.StandardCopyOption.*;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.util.List;


public class NewIOFileAction implements FileActionsInter{
	
	public void copyFile(File src,File dst){
		Path file = src.toPath();
		Path f2 = dst.toPath();
		try{
			Files.copy(file,f2);
		}catch(IOException e){
			System.err.println("Okurrr: "+e);
		}
	}
	
	public void moveFile(File src,File dst){
		Path file = src.toPath();
		Path f2 = dst.toPath();
		
		try{
			Files.move(file, f2, REPLACE_EXISTING);
		}catch(IOException e){
			System.err.println(e);
		}
		src.delete();
	}
	
	
	
	
	public void renameFile(File file, String newName){
		Path src = file.toPath();
		Path dst = Paths.get(newName);
		
		try{
			Files.move(src, dst, REPLACE_EXISTING);
		}catch(IOException e){
			System.err.println(e);
		}
	}


	public boolean checkFile(File file){return true;};
	
	public boolean containsString(File src, String sampleString){
		String content="";
		Path file = src.toPath();
		
		try(InputStream in = Files.newInputStream(file)){
			int b;
			int i=0;
			while((b=in.read()) != -1){
				char c = (char) b;
				content=content+c;
			}
		}catch(IOException e){
			System.err.println(e);
		}
		
		//CHECK IF CONTENT CONTAINS STRING
		return content.contains(sampleString);
	};
	
	public int firstOccurenceInFile(File src, String sampleString){
		String content="";
		Path file = src.toPath();
		
		try(InputStream in = Files.newInputStream(file)){
			int b;
			int i=0;
			while((b=in.read()) != -1){
				char c = (char) b;
				content=content+c;
			}
		}catch(IOException e){
			System.err.println(e);
		}
		
		//CHECK IF CONTENT CONTAINS STRING
		if(!content.contains(sampleString)){
			return -1;
		}else{
			//System.out.println(content.indexOf(sampleString));
			//CHECK STRING FIRST INDEX
			return content.indexOf(sampleString);
		}
	};
	
	public int lastOccurentInFile(File src, String sampleString){
		String content="";
		Path file = src.toPath();
		
		try(InputStream in = Files.newInputStream(file)){
			int b;
			int i=0;
			while((b=in.read()) != -1){
				char c = (char) b;
				content=content+c;
			}
		}catch(IOException e){
			System.err.println(e);
		}
		
		//CHECK IF CONTENT CONTAINS STRING
		if(!content.contains(sampleString)){
			return -1;
		}else{
			//System.out.println(content.lastIndexOf(sampleString));
			//CHECK STRING FIRST INDEX
			return content.lastIndexOf(sampleString);
		}
	};
	
	public void deleteFile(File file){
		Path files=file.toPath();
		if(Files.isDirectory(files)){
			
			try (DirectoryStream<Path> stream = Files.newDirectoryStream(files)) {
				for (Path x: stream) {
					if(Files.isDirectory(x)){
						//System.out.println(x.listFiles().length);
						if(getListOfFiles(x)!=0){
							String str=x.toAbsolutePath().toString();
							File files2=new File(str);
							deleteFile(files2);
							Files.delete(x);
						}else{
							//System.out.println(x.getName());
							Files.delete(x);
						}
					}else{
						Files.delete(x);
					}
					//System.out.println(x.getName());
				}
			}catch(IOException e){
				System.out.println(e);
			}
		}else{
			try{
				Files.delete(files);
			}catch(IOException e){
				System.out.println(e);
			}
		}
	};
	
	public static int getListOfFiles(Path x){
		List<String> fileNames = new ArrayList<>();
		try{
			DirectoryStream<Path> directoryStream = Files.newDirectoryStream(x);
			for (Path path : directoryStream) {
				fileNames.add(path.toString());
			}
		} catch (IOException e) {
			System.out.println(e);
		}
		return fileNames.size();
	}
	
	public File[] findFilesWithString(File directory, String sampleString){
		File[] files=null;
		return files;
	};
	
	public void copyStreamToStream(InputStream in, OutputStream out) throws IOException{}
}