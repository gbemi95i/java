import java.io.*;
public class ByteBuffer{
	
	public static void main(String[] args){
		File src=new File("src/A.txt");
		File dst=new File("dst/A.txt");
		
		//COPY FILE
		try(FileInputStream in=new FileInputStream(src);
			FileOutputStream out=new FileOutputStream(dst)){
			
			copy(in,out);
			System.out.println("Successful");
		}catch(IOException e){
			e.printStackTrace();
		}//YOU RUN THE RISK OF OPEN STREAMS (CATCH VS THROWS IN METHODS SIGNATURE)
	}
	
	
	public static void copy(InputStream in, OutputStream out) throws IOException{
		//COPY FILE
			int c;
			long t1=System.currentTimeMillis();
			byte[] b=new byte[1000];
			//int len=1000;
			
			//while((c=in.read(b,0,len)) != -1){
			IOException ioe=new IOException();
			/**if(true){
				throw ioe;
			}**/
			while((c=in.read(b)) != -1){
				out.write(b,0,c);
				//System.out.println("Did it");
				/**THE WRITE METHOD TAKES THE INT AND WRITES THE BYTE
					VALUE TO THE OUTPUT STREAM**/
			}
			long t2=System.currentTimeMillis();
			System.out.println(t2-t1+" milliseconds");//YOU RUN THE RISK OF OPEN STREAMS (CATCH VS THROWS IN METHODS SIGNATURE)
	}
}