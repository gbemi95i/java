import java.io.*;
public class Tempo{
	public static void main(String[] args) throws IOException{
		FileAction f=new FileAction();
		
		FileInputStream in=new FileInputStream("commands.txt");
		byte b[]=f.streamToBytes(in);
		
		String s=new String(b);
		
		System.out.println("String s: "+s);
		
		in.close();
		in=new FileInputStream("commands.txt");
		
		s=f.streamToString(in);
		
		System.out.println("String s: "+s);
	}
}