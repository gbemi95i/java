import java.io.*;
import java.util.Scanner;

public class ScanMyBytes2{
	public static void main(String[] args) throws IOException{
		//BufferStream in=null;
		//FileOutputStream out=null;
		//Scanner s=null;
		
		try(Scanner s = new Scanner(new BufferedInputStream(new FileInputStream("My Alphabets3.txt")));
			FileOutputStream out= new FileOutputStream("My Alphabets4.txt")){
			
			while(s.hasNext()){
				//out.write(s.nextInt());
				System.out.println(s.next());
			}
		}
	}
}