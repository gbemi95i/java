import java.io.*;
import java.io.IOException;
import java.nio.file.*;
import static java.nio.file.StandardCopyOption.*;
import java.net.URI;
public class ListFiles{
	public static void main(String[] args){
		Path f = Paths.get("./Exercises");
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(f)) {
			for (Path file: stream) {
				System.out.println(file.getFileName());
			}
		} catch (IOException | DirectoryIteratorException x) {
			// IOException can never be thrown by the iteration.
			// In this snippet, it can only be thrown by newDirectoryStream.
			System.err.println(x);
		}
	}
}