import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.Locale;

public class ScanAdd {
    public static void main(String[] args) throws IOException {

        Scanner s = null;
        int sum= 0;

        try {
            s = new Scanner(new BufferedReader(new FileReader("MyNums.txt")));
            s.useLocale(Locale.US);

            while (s.hasNext()){
                if(s.hasNextInt()){
                    sum += s.nextInt();
                }else{
                    s.next();
                }   
            }
        } finally {
            s.close();
        }

        System.out.println(sum);
    }
}