public class random2{
	//ALTERNATING EVEN & ODD NUMBERS
	public static void main(String[] args){
		int[] arr=new int[10];
		getRandNums(arr);//TODO: No need for the assignment
		for(int el:arr){
			System.out.println(el);
		}
	}
	
	public static void getRandNums(int[] arr){ 
		//TODO: Bad method name. Method 	names should normally be verbal e.g. doThis, showThat, printThose
		//TODO: I did not bother reading through. Your approach is slow and unnecessarily complex. Rethink the problem and redo.
		
		int i=0;
		int x;
		int limit=50;
		int prevNum;
		do{
			x=(int) (Math.random()*limit);
			if(i==0){
				arr[i]=x;
				i++;
			}else{
				prevNum=arr[i-1];
				if(prevNum%2==0 && x%2==0  ||  prevNum%2!=0 && x%2!=0){}
				else{
					arr[i]=x;
					i++;
				}
			}
		}while(i<arr.length);
	}
}