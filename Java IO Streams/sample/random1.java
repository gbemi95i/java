public class random1{
	//TO PRINT EVEN NUMBERS
	public static void main(String[] args){
		// int value=(int)(Math.random()*50)+1;
		// System.out.println(value);
		int[] nums=new int[6];
		
		//TODO: An array is an object in java. The changes performed in another method can be seen here; no need for the assignment i.e. nums = ...
		getRandNums(nums);
		
		for(int i:nums){
			System.out.println(i);
		}
	}
	
	public static void getRandNums(int[] arr){
		for(int i=0; i<arr.length; i++){
			arr[i]=(int) (Math.random()*50)+1;
		}
	}
}