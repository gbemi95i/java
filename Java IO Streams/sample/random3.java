import java.util.Arrays;
//TO PRINT ESCALATING RANDOM NUMBERS
public class random3{
	public static void main(String[] args){
		int[] arr=new int[20];
		getRandNums(arr);
		for(int el:arr){
			System.out.println(el);
		}
	}
	
	public static void getRandNums(int[] arr){
		//TODO: Your approach here, of generating and sorting is intelligent.
		//However, that is not what I want.
		//What you currently do is this: You generate the number using Math.random and then you perform some post-processing on it
		//In this case, you multiply by 50 and add 1
		//I want you to alter your post-processing to achieve the desired result; Not that you generate everything and then re-arrange
		//I don't want anything after the for loop.
		
		int y;
		int i=0;
		int limit=10;
		do{
			y=(int) (Math.random()*limit);
			if(i==0){
				arr[i]=y;
				i++;
			}else{
				if(y>arr[i-1]){//CHECKING IF THE NEW RANDOM NUMBER IS BIGGER THAN THE LAST NUMBER ADDED TO THE ARRAY
					arr[i]=y;
					i++;
				}else{//INCREASE THE LIMIT FOR A BETTER CHANCE
					limit+=1;
				}
			}
		}while(i<arr.length);
	}
}