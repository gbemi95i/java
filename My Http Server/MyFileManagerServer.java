import java.net.Socket;
import java.net.ServerSocket;
import java.io.*;
import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.net.SocketTimeoutException;
import java.net.URLDecoder;
class MyFileManagerServer{
	int portNum;
	String dir;
	MyFileManagerServer(int portNum, String dir){
		this.portNum=portNum;
		this.dir=dir;
	}
	
	void startConnection(){
		try{
			ServerSocket ss=new ServerSocket(portNum);
			while(true){
				Socket cs=ss.accept();
				start(cs);
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	void start(Socket ss) throws IOException{
		InputStream in=null;
		OutputStream out=null;
		try{
			in=ss.getInputStream();
			out=ss.getOutputStream();
			
			System.out.println("STARTED");
			byte[] b=new byte[10056];
			int readLength;
			int i=0;
			
			ss.setSoTimeout(3000);
			readLength=in.read(b);
			
			if(readLength==-1){
				//System.out.println("Error reading message");
				handleError(out);
			}else{
				handleRequest(out,b,readLength);
			}
			
			//System.out.println("messageLength="+readLength);
			//System.out.println("i="+i);
			
			
		}catch(SocketTimeoutException ste){
			handleError(out);
		}finally{
			out.close();
		}
	}
	
	void handleRequest(OutputStream out, byte[] b, int readLength) throws IOException{
		Scanner sc;
		byte[] headerByte;
		byte[] bodyByte=null;
		byte[] defaultt;
		String inputLine;
		
		inputLine=extractString(b, readLength);
		System.out.println("REQUEST"+inputLine);
		sc=new Scanner(inputLine);
		String startLine=sc.nextLine();
		String req=startLine.split(" ")[1];
		
		String lastLine=params(sc);
		System.out.println("Last line " + lastLine);
		String header="";
		
		if(!(lastLine.equals(""))){
			if(lastLine.equals("loadList")){
				sendFilesList(out);
			}else if(openRequest(lastLine)){
				String fileName=lastLine.split("=")[1];
				byte[] content=fetchFileContent(fileName);
				header="HTTP/1.1 200 OK \nContent-Type:text/plain \nContent-Length:"+content.length+" \n\n";
				headerByte=header.getBytes();
				
				out.write(headerByte);
				out.write(content);
			}else{
				System.out.println("GOT HERE:"+lastLine);
				Map<String,String> m=extractParams(lastLine);
				
				String data="";
				for(String key : m.keySet()){
					if(!key.equals("File name"))
						data=data+key+": "+m.get(key)+"\n";
				}
				
				saveParamsToFile(m.get("File name"),data);
				sendFilesList(out);
			}
		}else{
			if(req.equals("/")){
				req="/index.html";
				String mime=getMimeType(req);
				//System.out.println("MIME:"+mime);
				bodyByte=getContents(req);
				header="HTTP/1.1 200 OK \nContent-Type: "+mime+" \nContent-Length:"+bodyByte.length+" \n\n";
				headerByte=header.getBytes();
				
				out.write(headerByte);
				out.write(bodyByte);
			}else{
				String mime=getMimeType(req);
				System.out.println("MIME:"+mime);
				bodyByte=getContents(req);
				header="HTTP/1.1 200 OK \nContent-Type: "+mime+" \nContent-Length:"+bodyByte.length+" \n\n";
				headerByte=header.getBytes();
				//System.out.println("Content="+s);
				//b2=s.getBytes();
				
				out.write(headerByte);
				out.write(bodyByte);
			}
		};
		

		System.out.println("RESPONSE ENDED");
	}
	
	boolean openRequest(String param){
		if(param.split("=")[0].equals("fileName"))
			return true;
		return false;
	}
	
	void handleError(OutputStream out) throws IOException{
		byte[] b2;
		String header="HTTP/1.1 400 Bad Request \nContent-Type: text/html; charset=utf-8 \nKeep-Alive: timeout=20, max=3000 \n\n";
		b2=header.getBytes();
		
		out.write(b2);
		
		System.out.println("Got here");
		System.out.println(header);
	}
	
	byte[] getContents(String srcStr) throws IOException{
		srcStr=dir+srcStr;
		srcStr="."+srcStr;
		//System.out.println("REQUESTING:"+srcStr);
		File src=new File(srcStr);
		FileAction fa=new FileAction();
		return fa.extractContent(src);
		//return "";
	}
	
	String getMimeType(String srcStr) throws IOException{
		//System.out.println("EXTRACTING MIME TYPE:"+srcStr);
		String extension=srcStr.split("\\.")[1];
		//System.out.println("EXTENSION:"+extension);
		String mimeType="";
		if(extension.equals("html") || extension.equals("htm")){
			mimeType="text/html";
		}else if(extension.equals("css") || extension.equals("scss")){
			mimeType="text/css";
		}else if(extension.equals("js")){
			mimeType="text/javascript";
		}else if(extension.equals("jpg")){
			mimeType="image/jpeg";
		}else if(extension.equals("png")){
			mimeType="image/png";
		}else if(extension.equals("gif")){
			mimeType="image/gif";
		}
		
		
		return mimeType;
	}
	
	String extractString(byte[] b, int readLength){
		byte[] actualMessage=new byte[readLength];
		String message;
		int i=0;
		
		while(i!=actualMessage.length){
			actualMessage[i]=b[i];
			i++;
		}
		message=new String(actualMessage);
		
		return message;
	}
	
	void saveParamsToFile(String fileName,String data) throws IOException{
		String foldr=dir+"/files/";
		File directory=new File(foldr);
		if(!directory.exists())
			directory.mkdir();
		String dst="."+foldr+fileName+".txt";
		System.out.println(dst);
		File file=new File(dst);
		boolean irrel=file.createNewFile();
		FileWriter out=new FileWriter(file);
		out.write(data);
		out.close();
	}
	
	Map<String,String> extractParams(String lastLine) throws UnsupportedEncodingException{
		lastLine=URLDecoder.decode( lastLine, "UTF-8" );;
		System.out.println("PARAMETERS: "+lastLine);
		
		String[] sets=lastLine.split("&");
		List setsList=new ArrayList();
		Map<String,String> person=new HashMap<String,String>();
		for(String set: sets){
			person.put(set.split("=")[0],set.split("=")[1]);
		}
	
		return person;
	}
	
	void sendFilesList(OutputStream out) throws IOException{
		String list=getFileList();
		byte[] listByte=list.getBytes();
		
		String header="HTTP/1.1 200 OK \nContent-Type:text/plain \nContent-Length:"+listByte.length+" \n\n";
		byte[] headerByte=header.getBytes();
		
		out.write(headerByte);
		
		out.write(listByte);
	}
	
	byte[] fetchFileContent(String fileName) throws IOException{
		File file=new File("./"+dir+"/files/"+fileName);
		FileInputStream in=new FileInputStream(file);
		FileAction fa=new FileAction();
		byte[] content=fa.streamToBytes(in);
		in.close();
		
		return content;
	}
	
	String getFileList(){
		File folder=new File("./"+dir+"/files/");
		File[] files=folder.listFiles();
		String list="";
		int i=0;
		for(File x: files){
			list+=x.getName()+"\n";
		}
		
		return list;
	}
	
	String params(Scanner sc){
		String lastLine="";
		while(sc.hasNextLine()){
			lastLine=sc.nextLine();
		}
		
		return lastLine;
		
	}
}