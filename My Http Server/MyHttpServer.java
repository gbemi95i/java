import java.net.Socket;
import java.net.ServerSocket;
import java.io.*;
import java.util.Scanner;
import java.net.SocketTimeoutException;
class MyHttpServer{
	int portNum;
	String dir;
	MyHttpServer(int portNum, String dir){
		this.portNum=portNum;
		this.dir=dir;
	}
	
	void startConnection(){
		try{
			ServerSocket ss=new ServerSocket(portNum);
			while(true){
				Socket cs=ss.accept();
				start(cs);
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	void start(Socket ss) throws IOException{
		InputStream in=null;
		OutputStream out=null;
		try{
			in=ss.getInputStream();
			out=ss.getOutputStream();
			
			System.out.println("STARTED");
			byte[] b=new byte[10056];
			int readLength;
			int i=0;
			
			
			ss.setSoTimeout(3000);
			readLength=in.read(b);
			
			
			if(readLength==-1){
				//System.out.println("Error reading message");
				handleError(out);
			}else{
				handleRequest(out,b,readLength);
			}
			
			//System.out.println("messageLength="+readLength);
			//System.out.println("i="+i);
			
			
		}catch(SocketTimeoutException ste){
			handleError(out);
		}finally{
			out.close();
		}
	}
	
	void handleRequest(OutputStream out, byte[] b, int readLength) throws IOException{
		Scanner sc;
		byte[] headerByte;
		byte[] bodyByte=null;
		byte[] defaultt;
		String inputLine;
		
		inputLine=extractString(b, readLength);
		System.out.println("REQUEST"+inputLine);
		sc=new Scanner(inputLine);
		String startLine=sc.nextLine();
		String req=startLine.split(" ")[1];
		
		String header="";
		
		if(req.equals("/")){
			req="/index.html";
			String mime=getMimeType(req);
			//System.out.println("MIME:"+mime);
			bodyByte=getContents(req);
			header="HTTP/1.1 200 OK \nContent-Type: "+mime+" \nContent-Length:"+bodyByte.length+" \n\n";
			headerByte=header.getBytes();
			//System.out.println("Content="+s);
			//b2=s.getBytes();
			
			out.write(headerByte);
			out.write(bodyByte);
			
			/*String s="<html><head></head><body><h1>Hello World!</h1></body></html>";
			int bLen=s.getBytes().length;
			header="HTTP/1.1 200 OK \nContent-Type: text/html; charset=utf-8 \nContent-Length:"+bLen+"\n \n\n";
			s=header+s;
			System.out.println("Content="+s);
			defaultt=s.getBytes();
			out.write(defaultt);*/
		}else{
			String mime=getMimeType(req);
			System.out.println("MIME:"+mime);
			bodyByte=getContents(req);
			header="HTTP/1.1 200 OK \nContent-Type: "+mime+" \nContent-Length:"+bodyByte.length+" \n\n";
			headerByte=header.getBytes();
			//System.out.println("Content="+s);
			//b2=s.getBytes();
			
			out.write(headerByte);
			out.write(bodyByte);
		}


		System.out.println("RESPONSE ENDED");
	}
	
	void handleError(OutputStream out) throws IOException{
		byte[] b2;
		String header="HTTP/1.1 400 Bad Request \nContent-Type: text/html; charset=utf-8 \nKeep-Alive: timeout=20, max=3000 \n\n";
		b2=header.getBytes();
		
		out.write(b2);
		
		System.out.println("Got here");
		System.out.println(header);
	}
	
	byte[] getContents(String srcStr) throws IOException{
		srcStr=dir+srcStr;
		srcStr="."+srcStr;
		System.out.println("REQUESTING:"+srcStr);
		File src=new File(srcStr);
		FileAction fa=new FileAction();
		return fa.extractContent(src);
		//return "";
	}
	
	String getMimeType(String srcStr) throws IOException{
		//System.out.println("EXTRACTING MIME TYPE:"+srcStr);
		String extension=srcStr.split("\\.")[1];
		//System.out.println("EXTENSION:"+extension);
		String mimeType="";
		if(extension.equals("html") || extension.equals("htm")){
			mimeType="text/html";
		}else if(extension.equals("css") || extension.equals("scss")){
			mimeType="text/css";
		}else if(extension.equals("js")){
			mimeType="text/javascript";
		}else if(extension.equals("jpg")){
			mimeType="image/jpeg";
		}else if(extension.equals("png")){
			mimeType="image/png";
		}else if(extension.equals("gif")){
			mimeType="image/gif";
		}
		
		
		return mimeType;
	}
	
	String extractString(byte[] b, int readLength){
		byte[] actualMessage=new byte[readLength];
		String message;
		int i=0;
		
		while(i!=actualMessage.length){
			actualMessage[i]=b[i];
			i++;
		}
		message=new String(actualMessage);
		
		return message;
	}
}