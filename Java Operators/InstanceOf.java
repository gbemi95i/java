class InstanceOf{
	public static void main(String[] args){
		test1 obj1=new test1();
		test2 obj2=new test2();
		test2 obj3=new test3();
		
		System.out.println("obj1 an instanceof test1:"+(obj1 instanceof test1));
		System.out.println("obj1 an instanceof test2:"+(obj1 instanceof test2));
		System.out.println("obj1 an instanceof test3:"+(obj1 instanceof test3));
		System.out.println("obj2 an instanceof test1:"+(obj2 instanceof test1));
		System.out.println("obj2 an instanceof test2:"+(obj2 instanceof test2));
		System.out.println("obj2 an instanceof test3:"+(obj2 instanceof test3));
		System.out.println("obj3 an instanceof test1:"+(obj3 instanceof test1));
		System.out.println("obj3 an instanceof test2:"+(obj3 instanceof test2));
		System.out.println("obj3 an instanceof test3:"+(obj3 instanceof test3));
		
		/**if(obj1 instanceof test1){
			System.out.println("obj1 an instanceof test1:"+(obj1 instanceof test1));
		}**/
		//t,f,f,t,t,f,t,t,t
		// A PARENT CANNOT BE AN INSTANCE OF A CHILD
	}
}

	class test1{};
	class test2 extends test1{};
	class test3 extends test2 implements inter{};
	interface inter {}