public class Equality{
	public static void main(String[] args){
		String x=new String("sample");
		String y=new String("sample");
		
		String a="sample";
		String b="sample";
		System.out.println("Is x==y:"+(x==y));
		System.out.println("Is a==b:"+(a==b));
		System.out.println("Is x.equals(y):"+x.equals(y)+" OFCOURSE!");
		System.out.println("Is a.equals(b):"+a.equals(b)+" OFCOURSE!");
		System.out.println("Is x==a:"+(x==a));
		System.out.println("Is x.equals(a):"+x.equals(a)+" OFCOURSE!");
		System.out.println("Conclusion: The '==' is only true with Strings made by direct value assignment e.g.x='bla'");
	}
}