/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbcsample;

import java.util.List;
import java.util.ArrayList;
/**
 *
 * @author DAVID
 */
public class JdbcSample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JDBCTester jt=new JDBCTester();
        //jt.createTable("books");
        //jt.insertToTable("books","Ghost Of Tsushima");
        //jt.readTable("books");
        //jt.updateTable("books",2,"Ghost Of Tsushima");
        //jt.deleteFromTable("books", 7); 
        //jt.dropTable("books");
        //jt.preparedInsertions("books","Last crusade");
        
        List<String> titles=new ArrayList<>();
        titles.add("Once Upon A Time");
        titles.add("Marco Polo");
        titles.add("Ghost Of Tsushima");
        
        jt.preparedInsertions("books",titles);
        
        /*try(
                Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/bookshop","root","password");
                Statement stat=conn.createStatement();
            ){
            String queryStr="SELECT * FROM books";
            //String queryStr2="CREATE TABLE books2 (id int, title varchar(50))";
            ResultSet sqlResult=stat.executeQuery(queryStr);
            ResultSetMetaData resultMetaData=sqlResult.getMetaData();
            System.out.println(sqlResult.next());
            System.out.println(sqlResult.next());
            System.out.println("Current row number:"+sqlResult.getRow());
            System.out.println("Column count:"+resultMetaData.getColumnCount());
            //System.out.println();
            /*while(result.next()){
                
            }*/
        /*}catch(SQLException s){
            System.out.println(s);
        }*/
        // TODO code application logic here
    }
    
}
