/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbcsample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.sql.PreparedStatement;

/**
 *
 * @author DAVID
 */
class JDBCTester {
    Connection conn;
    Statement stat;
    
    JDBCTester(){
        try{
            conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/bookshop","root","password");
            stat=conn.createStatement();
        }catch(SQLException s){
                System.out.println(s);
        }
    }
    
    void createTable(String tableName){
        try{
            String queryStr="CREATE TABLE "+tableName+" (id int AUTO_INCREMENT, title varchar(50), PRIMARY KEY(id))";
            stat.executeUpdate(queryStr);
                
            System.out.println("Table "+tableName+" created");
        }catch(SQLException s){
            System.out.println(s);
        }
    }
    
    
    void insertToTable(String tableName, String newTitle){
        try{
            String queryStr="INSERT INTO "+tableName+" (title) VALUES ('"+newTitle+"')";
            stat.executeUpdate(queryStr);
        }catch(SQLException s){
            System.out.println(s);
        }
    }
    
    void readTable(String tableName){
        try{
            String queryStr="SELECT * FROM "+tableName;
            ResultSet queryResult=stat.executeQuery(queryStr);
            List<Map<String,String>> results = new ArrayList<>();
            ResultSetMetaData resMetData=queryResult.getMetaData();
            int numOfCol=resMetData.getColumnCount();
            String columnName;
            int colIndex;
            Map<String,String> row;
            while(queryResult.next()){
                row=new HashMap<>();
                colIndex=1;
                while(colIndex<=numOfCol){
                    columnName=resMetData.getColumnName(colIndex);
                    row.put(columnName, queryResult.getString(columnName));
                    //System.out.println("A: "+columnName);
                    //System.out.println("B: "+queryResult.getString(columnName));
                    colIndex++;
                }
                
                results.add(row);
            }
            
            printResult(results);
        }catch(SQLException s){
            System.out.println(s);
        }
    }
    
    void updateTable(String tableName, int id, String newTitle){
        try{
            String queryStr="UPDATE "+tableName+" SET title = '"+newTitle+"' WHERE id = '"+id+"' ";
            stat.executeUpdate(queryStr);
        }catch(SQLException s){
            System.out.println(s);
        }
    }
    
    void deleteFromTable(String tableName, int id){
        try{
            String queryStr="DELETE FROM "+tableName+" WHERE id = "+id;
            stat.executeUpdate(queryStr);
        }catch(SQLException s){
            System.out.println(s);
        }
    }
    
    void dropTable(String tableName){
        try{
            String queryStr="DROP TABLE "+tableName;
            stat.executeUpdate(queryStr);
        }catch(SQLException s){
            System.out.println(s);
        }
    }
    
    void preparedInsertion(String tableName,String title){
        try{
            String sqlCommand="INSERT INTO "+tableName+" (title) VALUES(?)";
            PreparedStatement ps=conn.prepareStatement(sqlCommand);
            ps.setString(1,title);
            ps.execute();
        }catch(SQLException s){
            System.out.println(s);
        }
    }
    
    void preparedInsertions(String tableName,List<String> titles){
        try{
            String sqlCommand="INSERT INTO "+tableName+" (title) VALUES(?)";
            PreparedStatement ps=conn.prepareStatement(sqlCommand);
            for(String title: titles){
                ps.setString(1,title);
                ps.addBatch();//THIS IS FASTER THAN EXECUTING EACH COMMAND AFTER EACH ITERATION
            }
            
            ps.executeBatch();
       }catch(SQLException s){
            System.out.println(s);
        }
    }
    
    void printResult(List<Map<String,String>> result){
        int rowIndex=0;
        while(rowIndex<result.size()){
            Map<String,String> row=result.get(rowIndex);
            
            for(String colName : row.keySet()){
                System.out.println(colName+": "+row.get(colName));
            }
            
            rowIndex++;
        }
    }
}
