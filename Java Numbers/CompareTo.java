public class CompareTo{
	public static void main(String[] args){
		Double d=7.565;
		Double d2=6.9;
		Float f=5.56f;
		Float f2=8.56f;
		Short s=15000;
		Short s2=15000;
		Long l=7L;
		String str="abc";
		String str2="def";
		
		/** d2=d;
		Number f2=f;**/
		System.out.println(d.compareTo(d2));
		System.out.println(s.compareTo(s2));
		System.out.println(f.compareTo(f2));
		System.out.println(str.compareTo(str2));
		//The compareTo method returns a negative integer if the first value
		//comes before the second value. It returns zero if the 2 values
		//being compared are equal. It returns a positive integer if the first
		//value comes after the second value.
	}
}