public class TypeCast{
	public static void main(String[] args){
		/**
			From byte>short>char>int>long>float>double
		**/
		int i=555;
		float f=i;
		System.out.println("initial int "+i);
		System.out.println("float "+i);
		//f=555+0.5f;
		i=(int) f;
		System.out.println("new int "+i);
	}
}