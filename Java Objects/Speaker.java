public class Speaker{
	static String model="";
	static int volume=0;
	static String mode="";
	
	public static void model(String m){
		model=m;
	}
	
	public static void increaseVol(int i){
		volume+=i;
	}
	
	public static void decreaseVol(int d){
		volume-=d;
	}
	
	public static void mode(String m){
		mode=m;
	}
	
	public static void printSpeakerStat(){
		System.out.println("Model:"+model+"\n"+"Volume:"+volume+"\n"+"Mode:"+mode);
	}
}