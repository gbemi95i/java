public class NewSpeaker{
	public static void main(String[] args){
		Speaker speaker1=new Speaker();
		speaker1.model("Mifa A10");
		speaker1.increaseVol(20);
		speaker1.decreaseVol(5);
		speaker1.mode("Radio");
		speaker1.printSpeakerStat();
	}
}