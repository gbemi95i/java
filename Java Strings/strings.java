public class strings{
	public static void main(String[] args){
		String sent1="ABCDEF";
		String sent2=sent1.toUpperCase();
		System.out.println(sent1);
		System.out.println(sent2);
		System.out.println("D is the "+sent1.indexOf('D'));
		System.out.println("Checkout my special characters using \\:");
		System.out.println("\' \" \\");
		System.out.println("Hello \t World \t Again");
		
		//ESCAPE SEQUENCES NEED A CHARACTER IN FRONT OF THEM TO DELETE WHAT'S BEHIND THEM
		System.out.println("No more 'o' Hello is now Hello\b ");
	}
}