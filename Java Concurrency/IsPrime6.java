import java.util.concurrent.atomic.AtomicInteger;
import java.util.ArrayList;
import java.util.Set;

public class IsPrime6 extends Thread{
	static int count=0;
	static ArrayList<Thread> lightThreads = new ArrayList<Thread>();
	static ArrayList<Integer> finishedThread = new ArrayList<Integer>();
	
	private static final Object lock = new Object();
	
	public static void main(String[] args){
		//Thread mainT = Thread.currentThread();
		
		int a=Integer.parseInt(args[0]);
		int b=Integer.parseInt(args[1]);
		
		long startTime = System.currentTimeMillis();
		/**try{
			t.join();
		}catch(InterruptedException e){
			System.out.println("HERE");
		}**/
		
		work(a,b);
		
		long endTime = System.currentTimeMillis();
		long timeElapsed = endTime-startTime;
		
		/*try{
			Thread.sleep(20000);
		}catch(InterruptedException ex){
			Thread.currentThread().interrupt();
		}*/
		
		System.out.println(count);
		System.out.println(timeElapsed);
	}
	
	public synchronized static void work(int a, int b){
		Sorter s = new Sorter(a,b);
		s.begin();
		
		int i=0;
		
		//System.out.println("S:"+lightThreads.size());
		
		//Set<Thread> threads = Thread.getAllStackTraces().keySet();
		 		
		/**for(Thread thr : lightThreads){
			//System.out.println("HERE2");
			if(thr.isAlive()){
				try{
					thr.join();
				}catch(InterruptedException e){}
			}
		}**/
		
		/*for (Thread t : lightThreads) {
			String name = t.getName();
			Thread.State state = t.getState();
			int priority = t.getPriority();
			String type = t.isDaemon() ? "Daemon" : "Normal";
			System.out.printf("%-20s \t %s \t %d \t %s\n", name, state, priority, type);
		}*/
		
		int j=0;
		while(finishedThread.size()!=3){
			Thread thr = lightThreads.get(j);
			Thread.State state = thr.getState();
			//if(!thr.isAlive()){
			if(state==Thread.State.TERMINATED){
				//System.out.println(state);
				finishedThread.add(1);
				j++;
				//if(j==3){
					//System.out.println("ALL DONE");
					//System.out.println(count);
				//}
			}
			/*try{
				/*synchronized(lock){
					lock.wait();
				}*/
			//}catch(InterruptedException e){}
		}
	}
}

class Sorter{
	int min;
	int max;
	
	Sorter(int min, int max){
		this.min=min;
		this.max=max;
	}
	
	public void allocate(int min, int max){
		int length = max-min;
		int[][] range = splitLength(min,max,length);
		
		/*for(int i=0; i<range.length; i++){
			for(int j=0; j<range[i].length; j++){
				System.out.println(range[i][j]);
			}
		}*/
		
		allocate2(range[0][0],range[0][1],"A");
		allocate2(range[1][0],range[1][1],"B");
		allocate2(range[2][0],range[2][1],"C");
		
		/**
		List<Thread> lightThreads = new ArrayList<Thread>();
		for(; min<max; min++){
			Thread t = new Thread(new IsPrimeThread(min));
			t.start();
			lightThreads.add(t);
		}
		**/
	}

	public void allocate2(int min, int max, String tag){
		Thread t = new Thread(new IsPrimeThread(min, max, tag));
		t.start();
		IsPrime6.lightThreads.add(t);
	}
	
	public int[][] splitLength(int min, int max, int length){
		int[][] x = new int[3][2];
		int set;
		
		set = length / 3;
		//System.out.println("Set2:"+set);
		
		int i=0;
		while(min<max){
			int max2=min+set;
			if(max2>max){
				max2=min+(max-min);
				x[i][0]=min;
				x[i][1]=max2;
			}else{
				x[i][0]=min;
				x[i][1]=max2;
			}
			i++;
			min=max2+1;//STARTING FROM THE NEXT NUMBER AFTER THE PREVIOUS RANGE
		}
		
		return x;
	}
	
	public void begin(){
		int m1 = this.min;
		int m2 = this.max;
		
		allocate(m1,m2);
	}
}

class IsPrimeThread implements Runnable{
	int min;
	int max;
	String tag;
	
	IsPrimeThread(int min, int max, String tag){
		this.min=min;
		this.max=max;
		this.tag=tag;
	}
	

	public void run(){
		//System.out.println("MIN"+min+"."+tag);
		//System.out.println("MAX"+max+"."+tag);
		check(min, max, tag);
	}
	
	public synchronized static void check(int x, int y, String tag){
		//System.out.println("MIN"+x+"."+tag);
		//System.out.println("MAX"+y+"."+tag);
		
		for(; x<=y; x++){
			if(isPrime(x)){
				//System.out.println("PRIME:"+x);
				IsPrime6.count+=1;
			}
			
			/*if(x==y){
				IsPrime6.finishedThread.add(1);
			}*/
		}
		
	}
	
	public synchronized static boolean isPrime(int x){
		double sqrt =  Math.sqrt(x);
		int s =  (int) Math.round(sqrt);
		//System.out.println("sqrt:"+s);
		
		if(x < 2 || x%2==0){
			return false;
		}
		
		for(int i=3; i<=s; i+=2){
			if(x%i==0){
				return false;
			}
		}
		
		//System.out.println(x+": true");
		return true;
	}
}