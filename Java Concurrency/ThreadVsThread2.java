public class ThreadVsThread2 extends Thread{
	int min;
	int max;
	
	
	ThreadVsThread2(int min, int max){
		this.min=min;
		this.max=max;
	}
	
	public static void main(String[] args){
		int m=0;
		int m2=100;
		
		ThreadVsThread2 s = new ThreadVsThread2(m, m2);
		ThreadVsThread2 s2 = new ThreadVsThread2(m+50, m2+50);
		s.start();
		s2.start();
	}
	
	public void run(){
		System.out.println(this.min);
		System.out.println(this.max);
	}
}