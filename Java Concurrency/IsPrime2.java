public class IsPrime2{
	public static void main(String[] args){
		int x=1;
		//boolean r = isPrime(x);
		//System.out.println(r);
		
		long startTime = System.currentTimeMillis();
		int r = range(1,250000);
		
		long endTime = System.currentTimeMillis();
		long timeElapsed = endTime-startTime;
		System.out.println(r);
		System.out.println(timeElapsed);
	}
	
	public static boolean isPrime(int x){
		if(x<=1){
			return false;
		}
		else{
			for(int i=0; i<x; i++){
				if(i!=0 && i!=1){
					if(x%i==0){
						return false;
					}
				}
			}
		}
		
		return true;
	}
	
	public static int range(int min, int max){
		int count=0;
		
		int min2=0;
		
		while(min2<max){
			min2=min+10000;
			if(min2>max){
				min2=min+(max-min);
				count+=range2(min,min2);
			}else{
				count+=range2(min,min2);
			}
			
			min=min2;
		}
		
		return count;
	}
	
	public static int range2(int min, int max){
		
		int count = 0;
		
		for(; min<=max; min++){
			if(isPrime(min)){
				count++;
			};
		}
		
		return count;
	}
}