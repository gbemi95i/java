public class GuardedBlocks{
	public static void main(String[] args){
		boolean x=false;
		//guardedJoy(x);
		
		Guarded g = new Guarded();
		g.guardedJoy(x);
	}
}

class Guarded{
	public synchronized void guardedJoy(boolean joy){
		// Simple loop guard. Wastes
		// processor time. Don't do this!

		while(!joy){
			try{
				wait();
			}catch(InterruptedException e){}
			System.out.println("WOW");
		}
		System.out.println("Joy has been achieved!");
	}
}