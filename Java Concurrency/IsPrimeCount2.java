import java.util.concurrent.atomic.AtomicInteger;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Set;

public class IsPrimeCount2{
	static int count=0;
	//static ArrayList<Thread> lightThreads=new ArrayList<Thread>();
	static ArrayList<Integer> finishedThread=new ArrayList<Integer>();
	static int threadSize = 4;
	//static volatile int[] countArray= new int[threadSize];
	static int totalCount= 0;
	
	private static final Object lock = new Object();
	
	public static void main(String[] args){
		int a=Integer.parseInt(args[0]);
		int b=Integer.parseInt(args[1]);
		
		//Arrays.fill(countArray, -1);
		
		int c = 1;
		if(args.length==3){
			c=Integer.parseInt(args[2]);
		}
		
		int totalExecTime=0;
		for(int i=0; i<c; i++){
			long startTime = System.currentTimeMillis();
			
			work(a,b);
			
			long endTime = System.currentTimeMillis();
			long timeElapsed = endTime-startTime;
			
			//count=countArray[0]+countArray[1]+countArray[2];
			
			System.out.println("Prime numbers found: "+totalCount+" Time taken: "+timeElapsed);
			
			totalExecTime+=timeElapsed;
			
			//Arrays.fill(countArray,0);
			totalCount=0;
			finishedThread.clear();
			
		}
		
		System.out.println("Total execution time: "+totalExecTime);
	}
	
	public synchronized static void work(int a, int b){
		Sorter2 s = new Sorter2(a,b);
		ArrayList<Thread> lightThreads=s.begin();
		
		int i=0;
		int j=0;
		int timesRan=0;
		
		//System.out.println(Arrays.toString(countArray));
		try{
			for(Thread t : lightThreads){
				t.join();
				timesRan++;
			}
		}catch(InterruptedException e){}
		
		//System.out.println("Times ran: "+timesRan);
	}
}

class Sorter2{
	ArrayList<Thread> lightThreads=new ArrayList<Thread>();
	int min;
	int max;
	
	Sorter2(int min, int max){
		this.min=min;
		this.max=max;
	}
	
	public void allocate(int min, int max){
		int[][] ranges = splitLength(min,max);
		
		int index = 0;
		for(int[] range : ranges){
			allocate2(range[0], range[1], index++);
		}
		
		/*allocate2(range[0][0],range[0][1],0);
		allocate2(range[1][0],range[1][1],1);
		allocate2(range[2][0],range[2][1],2);*/
		
	}

	public void allocate2(int min, int max, int tag){
		Thread t = new Thread(new IsPrimeThread2(min, max, tag));
		lightThreads.add(t);
		t.start();
	}
	
	public int[][] splitLength(int min, int max){
		int length = max-min;
		int[][] x = new int[IsPrimeCount2.threadSize][2];
		int set;
		
		set = length / IsPrimeCount2.threadSize;
		
		int i=0;
		while(min<max){
			int max2=min+set;
			if(max2>max){
				max2=min+(max-min);
				x[i][0]=min;
				x[i][1]=max2;
			}else{
				x[i][0]=min;
				x[i][1]=max2;
			}
			i++;
			min=max2+1;//STARTING FROM THE NEXT NUMBER AFTER THE PREVIOUS RANGE
		}
		
		return x;
	}
	
	public ArrayList<Thread> begin(){
		int m1 = this.min;
		int m2 = this.max;
		
		allocate(m1,m2);
		
		return lightThreads;
	}
}

class IsPrimeThread2 implements Runnable{
	int min;
	int max;
	int tag;
	int tCount=0;

	IsPrimeThread2(int min, int max, int tag){
		this.min=min;
		this.max=max;
		this.tag=tag;
	}
	

	public void run(){
		check(min, max, tag);
		//IsPrimeCount2.countArray[tag]=tCount;
		addUp(tCount);
	}
	
	public void check(int x, int y, int tag){
		
		for(; x<=y; x++){
			if(isPrime(x)){
				tCount+=1;
			}
		}
		
	}
	
	public boolean isPrime(int x){
		double sqrt =  Math.sqrt(x);
		int s =  (int) Math.round(sqrt);
		
		if(x < 2 || x%2==0){
			return false;
		}
		
		for(int i=3; i<=s; i+=2){
			if(x%i==0){
				return false;
			}
		}
		
		return true;
	}
	
	public synchronized static void addUp(int tCount){
		IsPrimeCount2.totalCount+=tCount;
	}
}