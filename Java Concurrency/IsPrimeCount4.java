import java.util.concurrent.*;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Set;

public class IsPrimeCount4{
	static int count=0;
	static ArrayList<Integer> finishedThread=new ArrayList<Integer>();
	static int threadSize = 1;
	static int totalCount= 0;
	static ExecutorService poolService=Executors.newFixedThreadPool(3);
	
	private static final Object lock = new Object();
	
	public static void main(String[] args){
		int a=Integer.parseInt(args[0]);
		int b=Integer.parseInt(args[1]);
		
		int c = 3;
		if(args.length==3){
			c=Integer.parseInt(args[2]);
		}
		
		int totalExecTime=0;
		for(int i=0; i<c; i++){
			long startTime = System.currentTimeMillis();
			
			work(a,b);
			
			long endTime = System.currentTimeMillis();
			long timeElapsed = endTime-startTime;
			
			System.out.println("Prime numbers found: "+totalCount+" Time taken: "+timeElapsed);
			
			totalExecTime+=timeElapsed;
			
			totalCount=0;
			
		}
		
		poolService.shutdownNow();
		System.out.println("Total execution time: "+totalExecTime);
	}
	
	public synchronized static void work(int a, int b){
		Sorter4 s = new Sorter4(a,b);
		ArrayList <Future<?>> futureObjects=s.begin();
		
		int timesRan=0;
		
		System.out.println(futureObjects.size());
		
		try{
			for(Future f : futureObjects){
				f.get();
				timesRan++;
			}
		}catch(InterruptedException i){}
		catch(ExecutionException e){}
		
		System.out.println("Times ran: "+timesRan);
	}
}

class Sorter4{
	ArrayList<Future<?>> futureObjects=new ArrayList<Future<?>>();
	int min;
	int max;
	
	Sorter4(int min, int max){
		this.min=min;
		this.max=max;
	}
	
	public void allocate(int min, int max){
		int[][] ranges = splitLength(min,max);
		
		/*for(int i=0; i<ranges.length; i++){
			for(int j=0; j<ranges[i].length; j++){
				System.out.println(ranges[i][j]);
			}
		}*/
		
		int index = 0;
		for(int[] range : ranges){
			allocate2(range[0], range[1], index++);
		}
		
		/*allocate2(range[0][0],range[0][1],0);
		allocate2(range[1][0],range[1][1],1);
		allocate2(range[2][0],range[2][1],2);*/
		
	}

	public void allocate2(int min, int max, int tag){
		Runnable t = new IsPrimeThread4(min, max, tag);
		Future<?> future=IsPrimeCount4.poolService.submit(t);
		futureObjects.add(future);
	}
	
	public int[][] splitLength(int min, int max){
		int length = max-min;
		int[][] x = new int[IsPrimeCount4.threadSize][2];
		int set;
		int set2;
		int tSize=IsPrimeCount4.threadSize;
		
		set = length / tSize;
		
		int i=0;
		int divider=1;
		
		while(min<max){
			divider=(int) (divider*2);
			
			set2 = set / divider;
			
			int max2=min+set+set2;
			if(max2>max){
				max2=min+(max-min);
				x[i][0]=min;
				x[i][1]=max2;
			}else{
				x[i][0]=min;
				x[i][1]=max2;
			}
			
			i++;
			min=max2+1; //STARTING FROM THE NEXT NUMBER AFTER THE PREVIOUS RANGE
		}
		
		return x;
	}
	
	public ArrayList<Future<?>> begin(){
		int m1 = this.min;
		int m2 = this.max;
		
		allocate(m1,m2);
		
		return futureObjects;
	}
}

class IsPrimeThread4 implements Runnable{
	int min;
	int max;
	int tag;
	int tCount=0;

	IsPrimeThread4(int min, int max, int tag){
		this.min=min;
		this.max=max;
		this.tag=tag;
	}
	

	public void run(){
		long startTime = System.currentTimeMillis();
		
		check(min, max, tag);
		
		long endTime = System.currentTimeMillis();
		long timeElapsed = endTime-startTime;
		System.out.println("Thread "+tag+": "+timeElapsed+"; Count: "+tCount);
		addUp(tCount);
	}
	
	public void check(int x, int y, int tag){
		
		for(; x<=y; x++){
			if(isPrime(x)){
				tCount+=1;
			}
		}
		
	}
	
	public boolean isPrime(int x){
		double sqrt =  Math.sqrt(x);
		int s =  (int) Math.round(sqrt);
		
		if(x < 2 || x%2==0){
			return false;
		}
		
		for(int i=3; i<=s; i+=2){
			if(x%i==0){
				return false;
			}
		}
		
		return true;
	}
	
	public synchronized static void addUp(int tCount){
		IsPrimeCount4.totalCount+=tCount;
	}
}