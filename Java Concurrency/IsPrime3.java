public class IsPrime3{
	public static void main(String[] args){
		//int x=1;
		//boolean r = isPrime(x);
		//System.out.println(r);
		
		 int a=Integer.parseInt(args[0]);
		 int b=Integer.parseInt(args[1]);
		 
		int c;
		if(args.length==3){
			c=Integer.parseInt(args[2]);
		}else{
			c=5;
		}
		
		// long startTime = System.currentTimeMillis();
		// int r = range(a,b);
		
		// long endTime = System.currentTimeMillis();
		// long timeElapsed = endTime-startTime;
		// System.out.println(r);
		// System.out.println(timeElapsed);
	
		int totalExecTime=0;
		int count=0;
		for(int i=0; i<c; i++){
			long startTime = System.currentTimeMillis();
			
			int r = range(a,b);
			
			long endTime = System.currentTimeMillis();
			long timeElapsed = endTime-startTime;
			
			count+=r;
			
			System.out.println("Prime numbers found: "+r+" Time taken: "+timeElapsed);
			
			totalExecTime+=timeElapsed;
		}
		
		System.out.println("Total execution time: "+totalExecTime);
	}
	
	public static boolean isPrime(int x){
		double sqrt =  Math.sqrt(x);
		int s =  (int) Math.round(sqrt);
		//System.out.println("sqrt:"+s);
		
		if(x < 2 || x%2==0){
			return false;
		}
		
		for(int i=3; i<=s; i+=2){
			if(x%i==0){
				return false;
			}
		}
		
		//System.out.println(x+": true");
		return true;
	}
	
	public static int range(int min, int max){
		
		int count = 0;
		
		for(; min<=max; min++){
			if(isPrime(min)){
				count++;
			};
		}
		
		return count;
	}
}