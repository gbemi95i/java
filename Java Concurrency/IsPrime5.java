import java.util.concurrent.atomic.AtomicInteger;

public class IsPrime5 extends Thread{
	static int count=0;
	
	public static void main(String[] args){
		//Thread mainT = Thread.currentThread();
		
		int a=Integer.parseInt(args[0]);
		int b=Integer.parseInt(args[1]);
		

		Thread t = new Thread(new Prime(a,b));
		//Thread t2 = new Thread(new IsPrime());

		
		/*for(int i=0; i<10; i++){
			System.out.println("AAAAAAAAAAA"+i);
		}*/

		long startTime = System.currentTimeMillis();
		
		t.start();
		try{
			t.join();
			Thread.sleep(1000);
		}catch(InterruptedException e){
			System.out.println("HERE");
		}
		
		long endTime = System.currentTimeMillis();
		long timeElapsed = endTime-startTime;
		
		System.out.println(count);
		System.out.println(timeElapsed);
	}
	
	public synchronized static void increaseCount(int num){
		if(isPrime(num)){
			count+=1;
		};
	}
	
	public static boolean isPrime(int x){
		double sqrt =  Math.sqrt(x);
		int s =  (int) Math.round(sqrt);
		//System.out.println("sqrt:"+s);
		
		if(x < 2 || x%2==0){
			return false;
		}
		
		for(int i=3; i<=s; i+=2){
			if(x%i==0){
				return false;
			}
		}
		
		//System.out.println(x+": true");
		return true;
	}
}

class Prime implements Runnable{
	int min;
	int max;
	
	Prime(int min, int max){
		this.min=min;
		this.max=max;
	}
	
	public void allocate(int min, int max){
		for(; min<max; min++){
			Thread t = new Thread(new IsPrimeThread(min));
			t.start();
		}
	}
	
	public void run(){
		int m1 = this.min;
		int m2 = this.max;
		
		allocate(m1,m2);
	}
}

class IsPrimeThread implements Runnable{
	int num;
	
	IsPrimeThread(int num){
		this.num=num;
	}
	

	public void run(){
		IsPrime5.increaseCount(this.num);
	}
}