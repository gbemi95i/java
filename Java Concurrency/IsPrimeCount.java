import java.util.concurrent.atomic.AtomicInteger;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Set;

public class IsPrimeCount extends Thread{
	static int count=0;
	static ArrayList<Thread> lightThreads = new ArrayList<Thread>();
	static ArrayList<Integer> finishedThread = new ArrayList<Integer>();
	static int[] countArray = new int[3];
	
	private static final Object lock = new Object();
	
	public static void main(String[] args){
		int a=Integer.parseInt(args[0]);
		int b=Integer.parseInt(args[1]);
		
		int c;
		if(args.length==3){
			c=Integer.parseInt(args[2]);
		}else{
			c=5;
		}
		
		int totalExecTime=0;
		for(int i=0; i<c; i++){
			long startTime = System.currentTimeMillis();
			
			work(a,b);
			
			long endTime = System.currentTimeMillis();
			long timeElapsed = endTime-startTime;
			
			count=countArray[0]+countArray[1]+countArray[2];
			
			System.out.println("Prime numbers found: "+count+" Time taken: "+timeElapsed);
			
			totalExecTime+=timeElapsed;
			
			Arrays.fill(countArray,0);
			count=0;
			lightThreads.clear();
			finishedThread.clear();
			
		}
		
		System.out.println("Total execution time: "+totalExecTime);
	}
	
	public synchronized static void work(int a, int b){
		Sorter s = new Sorter(a,b);
		s.begin();
		
		int i=0;
		int j=0;
		int timesRan=0;
		while(finishedThread.size()!=3){
			Thread thr = lightThreads.get(j);
			Thread.State state = thr.getState();
			if(state==Thread.State.TERMINATED){
				finishedThread.add(1);
				j++;
			}
			
			//timesRan++;
		}
		
		//System.out.println("Times ran: "+timesRan);
	}
}

class Sorter{
	int min;
	int max;
	
	Sorter(int min, int max){
		this.min=min;
		this.max=max;
	}
	
	public void allocate(int min, int max){
		int length = max-min;
		int[][] range = splitLength(min,max,length);
		
		allocate2(range[0][0],range[0][1],0);
		allocate2(range[1][0],range[1][1],1);
		allocate2(range[2][0],range[2][1],2);
		
	}

	public void allocate2(int min, int max, int tag){
		Thread t = new Thread(new IsPrimeThread(min, max, tag));
		t.start();
		IsPrimeCount.lightThreads.add(t);
	}
	
	public int[][] splitLength(int min, int max, int length){
		int[][] x = new int[3][2];
		int set;
		
		set = length / 3;
		
		int i=0;
		while(min<max){
			int max2=min+set;
			if(max2>max){
				max2=min+(max-min);
				x[i][0]=min;
				x[i][1]=max2;
			}else{
				x[i][0]=min;
				x[i][1]=max2;
			}
			i++;
			min=max2+1;//STARTING FROM THE NEXT NUMBER AFTER THE PREVIOUS RANGE
		}
		
		return x;
	}
	
	public void begin(){
		int m1 = this.min;
		int m2 = this.max;
		
		allocate(m1,m2);
	}
}

class IsPrimeThread implements Runnable{
	int min;
	int max;
	int tag;
	
	IsPrimeThread(int min, int max, int tag){
		this.min=min;
		this.max=max;
		this.tag=tag;
	}
	

	public void run(){
		check(min, max, tag);
	}
	
	public void check(int x, int y, int tag){
		
		for(; x<=y; x++){
			if(isPrime(x)){
				IsPrimeCount.countArray[tag]+=1;
			}
		}
		
	}
	
	public synchronized boolean isPrime(int x){
		double sqrt =  Math.sqrt(x);
		int s =  (int) Math.round(sqrt);
		
		if(x < 2 || x%2==0){
			return false;
		}
		
		for(int i=3; i<=s; i+=2){
			if(x%i==0){
				return false;
			}
		}
		
		return true;
	}
}