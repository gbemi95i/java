public class IsPrime{
	public static void main(String[] args){
		int x=1;
		//boolean r = isPrime(x);
		//System.out.println(r);
		int a=Integer.parseInt(args[0]);
		int b=Integer.parseInt(args[1]);
		
		long startTime = System.currentTimeMillis();
		int r = range(a,b);
		
		long endTime = System.currentTimeMillis();
		long timeElapsed = endTime-startTime;
		System.out.println(r);
		System.out.println(timeElapsed);
	}
	
	public static boolean isPrime(int x){
		if(x<=1){
			return false;
		}
		else{
			for(int i=0; i<x; i++){
				if(i!=0 && i!=1){
					if(x%i==0){
						return false;
					}
				}
			}
		}
		
		return true;
	}
	
	public static int range(int min, int max){
		
		int count = 0;
		
		for(; min<=max; min++){
			if(isPrime(min)){
				count++;
			};
		}
		
		return count;
	}
}