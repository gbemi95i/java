public class ThreadVsThread implements Runnable{
	String name;
	
	ThreadVsThread(String s){
		this.name=s;
	}
	
	public static void main(String[] args){
		Thread t = new Thread(new ThreadVsThread("A"));
		Thread t2 = new Thread(new ThreadVsThread("B"));
		t.start();
		t2.start();
		
	}
	
	public void run(){
		for(int i=0; i<21; i++){
			System.out.println(this.name);
		}
	}
}