public class AnonLambda{
	public static void main(String[] args){
		/*Anon2 a=()->{
			System.out.println("I was not like this before");
		};*/
		//LAMBDA IS SOLELY FOR SINGLE METHOD INTERFACES
		
		AnonInterface ai=()->{
			System.out.println("I am the new lambda on the block!");
			System.out.println("All the way from single method interface");
		};
		
		//a.act();
		//a.start();
		ai.start();
	}
}