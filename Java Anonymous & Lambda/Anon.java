public class Anon{
	public static void main(String[] args){
		Anon2 a=new Anon2(){
			public void act(){
				System.out.println("Anon.act");
			}
			
			public void start(){
				System.out.println("I was not like this before");
				act();
			}
		};
		
		AnonInterface ai=new AnonInterface(){
			public void act(){
				System.out.println("AnonInterface.act");
			}
			
			public void start(){
				System.out.println("I am new");
				act();
			}
		};
		
		//a.act();
		a.start();
		ai.start();
	}
}