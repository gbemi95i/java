import java.util.Arrays;
public class myMergeSortTest{
	public static void main(String[] args){
		int[] nums={36,8,4,97,3,91,10,3,4,37,59,2,5,67,21,45,4,97,3,91,10,3,4,37,5,1,2,3,4,6,4,37};
		int[] finalArr=new int[nums.length];
		
		int[][] splittedArr=arrSplit(nums);
		
		//MERGE SORT
		mergeSort(splittedArr,finalArr);
		
		//DISPLAY RESULT
		//System.out.println("Final Set:");
		for(int p : finalArr){
			System.out.println(p);
		}
	}
	
	public static int[][] arrSplit(int[] arr){
		//SPLIT ARRAYS COMPLETELY
		int[][] singledElements=new int[arr.length][1];
		for(int i=0; i<arr.length; i++){
			int[] singleValue=new int[1];
			singleValue[0]=arr[i];
			singledElements[i]=singleValue;
		}
		
		return singledElements;
	}
	
	public static void mergeSort(int[][] broken,int[] fArr){
		int numOfSets;
		int[][] sets;
		
		//DECIDE THE NUMBER OF SET(S) OF ARRAYS TO BE MADE USING THE NATURE OF NUMBER OF ELEMENTS THE 'BROKEN' ARRAY--EVEN OR ODD
		if(broken.length%2==0){
			numOfSets=broken.length/2;
			sets=new int[numOfSets][];
		}else{
			numOfSets=(broken.length/2)+1;
			sets=new int[numOfSets][];
		}
		int i,j,k,m;
		m=0;
		for(i=0; i<broken.length; i++){
			int[] set;
			if(i!=broken.length-1){
				int setLength=broken[i].length+broken[i+1].length;
				set=new int[setLength];
				j=0;
				
				int[] firstArray=broken[i];
				int[] secondArray=broken[i+1];
				
				set=mergeSort(firstArray,secondArray);
				
				//MERGE ARRAYS IN TWOS. TWO AFTER TWO.
				/**for(k=0; k<broken[i].length; k++){
					//System.out.println("I="+i);
					set[j]=broken[i][k];
					j++;
				}
				for(k=0; k<broken[i+1].length; k++){
					//System.out.println(i+1);
					set[j]=broken[i+1][k];
					j++;
				}**/
				i++;
				j=0;
			}else{//ONLY APPLICABLE FOR LAST ARRAY IF LENGTH IS ODD
			System.out.println("I'M RUNNING");
				int setLength=broken[i].length;
				set=new int[setLength];
				for(k=0; k<broken[i].length; k++){
					//System.out.println(broken[i][k]);
					set[k]=broken[i][k];
				}
			}
			
			System.out.println("ADDING NEW SET" + Arrays.toString(set));
			sets[m]=set;
			m++;
		}
		
		// if(sets.length==2){
			// for(int a=0; a<sets.length; a++){
				// System.out.println("New set:");
				// for(int b=0; b<sets[a].length; b++){
					// System.out.println(sets[a][b]);
				// }
			// }
		// }
		
		//SORT THE JUST CONCLUDED MERGED SETS
		/*for(int a=0; a<sets.length; a++){
				//System.out.println("New set:");
				for(int b=0; b<sets[a].length; b++){
					if(b!=(sets[a].length)-1){
						int p=sets[a][b];
						int q=sets[a][b+1];
						if(p>q){
							sets[a][b]=q;
							sets[a][b+1]=p;
							b=-1;
						}
					}
					//System.out.println(sets[a][b]);
				}
		}*/
		
		//KEEP GOING TILL ALL ARE IN A ONE SIZED ARRAY 
		if(sets.length!=1){
			mergeSort(sets,fArr);
		}else{
			for(int x=0; x<sets[0].length; x++){
				fArr[x]=sets[0][x];
				//System.out.println(sets[0][x]);
			}
			
			/**for(int p : fArr){
				System.out.println(p);
			}**/
		}
	}
	
	public static int[] mergeSort(int[] arr1,int[] arr2){
		int[] allSortedNums=new int[arr1.length+arr2.length];
		int a=0;
		int b=0;
		int c=0;
		while(a<allSortedNums.length){
			if(b<arr1.length && c<arr2.length){
				if(arr1[b]<=arr2[c]){
					allSortedNums[a]=arr1[b];
					b++;
				}else{
					allSortedNums[a]=arr2[c];
					c++;
				}
				
			}else{
				if(b==arr1.length){
					copyTheRest(arr2,allSortedNums,c,a);
				}
				else{
					copyTheRest(arr1,allSortedNums,b,a);
				}
				break;
			}
			a++;
		}
		
		return allSortedNums;
	}
	
	public static void copyTheRest(int[] from, int[] to, int index,int index2){
		System.out.println("COPY THE REST");
		for(index2=index2; index2<to.length; index2++){
			to[index2]=from[index];
			index++;
		}
	}
	
}