public class MyQuickSorter{
	public static void main(String[] args){
		int[] nums={36,8,4,97,3,91,10,3,497,3,91,10,3,4,37,5,1,2,3,4,6,4,37};
		int[] finalArr=new int[nums.length];
		
		//int[][] splittedArr=arrSplit(nums);
		
		//QUICKSORT
		quickSort(nums);
		
		//DISPLAY RESULT
		System.out.println("Final Set:");
		for(int p : nums){
			System.out.println(p);
		}
	}
	
	public static void quickSort(int[] arr){
		while(!sorted(arr)){
			int randPivot=Math.rand()*arr.length;
			sortSplit(arr,pivot);
		}
		
		
		//DUPLICATE UNSORTED ARRAY
		int[] initialArr=new int[arr.length];
		int i;
		for(i=0; i<initialArr.length; i++){
			initialArr[i]=arr[i];
		}
		
		int hal
		//QUICKSORT ARRAY USING EVERY ELEMENT PRESENT AS PIVOT ENSURING ACCURATE SORT (starting from the last element)
		for(i=arr.length-1; i>=0; i--){
			int pivot=initialArr[i];
			sortSplit(arr,pivot);
		}
		
		for(i=0; i<arr.length; i++){
			System.out.println(arr[i]);
		}
	}
	
	public static void sortSplit(int[] arr,int pivot){
		//SORT
		//System.out.println("PIVOT="+pivot);
		int[][] partitions=new int[2][];
		int amountLesser=0;
		int amountGreater=0;
		for(int a=0; a<arr.length; a++){
			if(arr[a]<=pivot){
				amountLesser++;
			}else{
				amountGreater++;
			}
		}
		
		/**SPLITTING NUMBERS IN COMPARISM TO PIVOT
		(less/equalTo pivot or greater than)**/
		int lesserIter=0;
		int greaterIter=0;
		partitions[0]=new int[amountLesser];
		partitions[1]=new int[amountGreater];
		int i;
		for(i=0; i<arr.length; i++){
			if(arr[i]<=pivot){
				partitions[0][lesserIter]=arr[i];
				lesserIter++;
			}else{
				partitions[1][greaterIter]=arr[i];
				greaterIter++;
			}
		}
		
		//SORTING PARTITIONS TO BE REPASSED IF NECESSARY
		int arrIter=0;
		for(i=0; i<partitions.length; i++){
			if(partitions[i].length!=0){
				for(int j=0; j<partitions[i].length; j++){
					//System.out.println(partitions[i][j]);
					arr[arrIter]=partitions[i][j];
					arrIter++;
				}
			}
		}
		
		/**for(i=0; i<arr.length; i++){
			System.out.println(arr[i]);
		}**/
	}
}