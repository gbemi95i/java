import java.util.Arrays;

public class ArraySortTester{
	public static void main(String[] args){
		//generalTest();
	
		/**timeTest(15);
		timeTest(150);
		timeTest(500);
		timeTest(1500);
		timeTest(5000);**/
		//timeTest(150);
		timeTest(50000);
	}

	public static void timeTest(int length){
		int limit=50;
		int[] arr=getRandomNums(length,limit);
		int[] arr2=arr.clone();
		int[] arr3=arr.clone();
		int[] arr4=arr.clone();
		
		long startingTime=System.currentTimeMillis();
		
		mergeSortRandomNums(arr);
		long mergeSortTime=System.currentTimeMillis();
		
		quickSortRandomNums(arr2);
		long quickSortTime=System.currentTimeMillis();
		
		//simpleSortRandomNums(arr3);
		//long simpleSortTime=System.currentTimeMillis();
		
		// badSortRandomNums(arr4);
		// long badSortTime=System.currentTimeMillis();
		
		System.out.println("Array Size: "+length);
		
		System.out.println("\nMergeSort");
		System.out.println("Valid: "+checkSortedNums(arr));
		System.out.println("Time taken: "+(mergeSortTime-startingTime));
		//System.out.println(Arrays.toString(arr));
		
		System.out.println("\nQuickSort");
		System.out.println("Valid: "+checkSortedNums(arr2));
		System.out.println("Time taken: "+(quickSortTime-mergeSortTime));
		//System.out.println(Arrays.toString(arr2));

		//System.out.println("\nSimpleSort");
		//System.out.println("Valid: "+checkSortedNums(arr3));
		//System.out.println("Time taken: "+(simpleSortTime-quickSortTime));
		//System.out.println(Arrays.toString(arr3));
		
		// System.out.println("\nBadSort");
		// System.out.println("Valid: "+checkSortedNums(arr4));
		// System.out.println("Time taken: "+(badSortTime-quickSortTime));
		//System.out.println(Arrays.toString(arr4));
		
		System.out.println("\n\n");
	
	}

	public static void generalTest(){
		int[] randomNums=getRandomNums(21,89);
		int[] randomNums2=getRandomNums(21,89);
		int[] randomNums3=getRandomNums(21,89);
		int[] randomNums4=getRandomNums(21,89);
		
		mergeSortRandomNums(randomNums);
		
		quickSortRandomNums(randomNums2);
		
		simpleSortRandomNums(randomNums3);
		
		badSortRandomNums(randomNums4);
		
		boolean stat;
		System.out.println("MERGE SORT:");
		stat=checkSortedNums(randomNums);
		System.out.println(stat);
		
		System.out.println("QUICK SORT:");
		stat=checkSortedNums(randomNums2);
		System.out.println(stat);
		
		System.out.println("SIMPLE SORT:");
		stat=checkSortedNums(randomNums3);
		System.out.println(stat);
		
		System.out.println("BAD SORT:");
		stat=checkSortedNums(randomNums4);
		System.out.println(stat);
	}
	
	public static int[] getRandomNums(int length, int limit){
		randomArrProducer randomNumCreator=new randomArrProducer();
		int[] randNumsArr=randomNumCreator.generateRandomNums(length,limit);
		// for(int i:randNumsArr){
			// System.out.println(i);
		// }
		
		return randNumsArr;
	}
	
	public static void mergeSortRandomNums(int[] randNums){
		MyMergeSorter mergeSorter=new MyMergeSorter();
		mergeSorter.sortArray(randNums);
	}
	
	public static void quickSortRandomNums(int[] randNums){
		MyQuickSort quickSorter=new MyQuickSort();
		quickSorter.sortArray(randNums);
	}
	
	public static void simpleSortRandomNums(int[] randNums){
		SimpleSort simpleSorter=new SimpleSort();
		simpleSorter.sortArray(randNums);
	}

	public static void badSortRandomNums(int[] randNums){
		BadMergeSort badSorter=new BadMergeSort();
		badSorter.sortArray(randNums);
	}	
	/**public static boolean checkSortedNums(int[] sortedNums){
		//System.out.println("afterwardsz:");
		boolean sorted=false;
		for(int i=0; i<sortedNums.length; i++){
			//System.out.println(sortedNums[i]);
			
			if(i!=sortedNums.length-1){
				if(sortedNums[i]<=sortedNums[i+1]){
					sorted=true;
				}
			}else{
				if(sortedNums[i]>=sortedNums[i-1]){
					sorted=true;
				};
			}
		}
		
		return sorted;
	}**/
	
	public static boolean checkSortedNums(int[] sortedNums){
		for(int i=0; i<sortedNums.length - 1; i++){
			if(sortedNums[i] > sortedNums[i+1]){
				return false;
			}
		}		
		return true;
	}
	
	public static boolean test(){
		int[] randomNums=getRandomNums(20,50);
		
		mergeSortRandomNums(randomNums);
		
		return checkSortedNums(randomNums);
	}
}