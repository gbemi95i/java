public class MyMergeSorter{
	/**public static void main(String[] args){
		int[] nums={36,8,4,97,3,91,10,3,4,37,59,2,5,67,21,45,4,97,3,91,10,3,4,37,5,1,2,3,4,6,4,37};
		sortArray(nums);
	}**/
	
	public static void sortArray(int[] arr){
		int[] finalArr=new int[arr.length];
		
		int[][] splittedArr=arrSplit(arr);
		
		//MERGE SORT
		mergeSort(splittedArr,finalArr);
		
		//arr=finalArr;
		for(int i=0; i<finalArr.length; i++){
			arr[i]=finalArr[i];
		}
		//DISPLAY RESULT
		//System.out.println("Final Set:");
		// System.out.println("afterwards:");
		// for(int p : arr){
			// System.out.println(p);
		// }
	}
	
	public static int[][] arrSplit(int[] arr){
		//SPLIT ARRAYS COMPLETELY
		int[][] singledElements=new int[arr.length][1];
		for(int i=0; i<arr.length; i++){
			int[] singleValue=new int[1];
			singleValue[0]=arr[i];
			singledElements[i]=singleValue;
		}
		
		return singledElements;
	}
	
	public static void mergeSort(int[][] broken,int[] fArr){
		int numOfSets;
		int[][] sets;
		
		//DECIDE THE NUMBER OF SET(S) OF ARRAYS TO BE MADE USING THE NATURE OF NUMBER OF ELEMENTS THE 'BROKEN' ARRAY--EVEN OR ODD
		if(broken.length%2==0){
			numOfSets=broken.length/2;
			sets=new int[numOfSets][];
		}else{
			numOfSets=(broken.length/2)+1;
			sets=new int[numOfSets][];
		}
		int i,j,k,m;
		m=0;
		for(i=0; i<broken.length; i++){
			int[] set;
			if(i!=broken.length-1){
				int setLength=broken[i].length+broken[i+1].length;
				set=new int[setLength];
				
				int[] firstArray=broken[i];
				int[] secondArray=broken[i+1];
				
				//MERGE ARRAYS IN TWOS. TWO AFTER TWO.
				set=mergeSort(firstArray,secondArray);
				i++;
			}else{//ONLY APPLICABLE FOR LAST ARRAY IF LENGTH IS ODD
				set=broken[i];
			}
			
			sets[m]=set;
			m++;
		}
		
		//KEEP GOING TILL ALL ARE IN A ONE SIZED ARRAY 
		if(sets.length!=1){
			mergeSort(sets,fArr);
		}else{
			for(int x=0; x<sets[0].length; x++){
				fArr[x]=sets[0][x];
				//System.out.println(sets[0][x]);
			}
			
			/**for(int p : fArr){
				System.out.println(p);
			}**/
		}
	}
	
	public static int[] mergeSort(int[] arr1,int[] arr2){
		int[] allSortedNums=new int[arr1.length+arr2.length];
		int a=0;
		int b=0;
		int c=0;
		while(a<allSortedNums.length){
			if(b<arr1.length && c<arr2.length){
				if(arr1[b]<=arr2[c]){
					allSortedNums[a]=arr1[b];
					b++;
				}else{
					allSortedNums[a]=arr2[c];
					c++;
				}
				
			}else{
				if(b==arr1.length){
					copyTheRest(arr2,allSortedNums,c,a);
				}
				else{
					copyTheRest(arr1,allSortedNums,b,a);
				}
				break;
			}
			a++;
		}
		
		return allSortedNums;
	}
	
	public static void copyTheRest(int[] from, int[] to, int index,int index2){
		for(index2=index2; index2<to.length; index2++){
			to[index2]=from[index];
			index++;
		}
	}
	
}