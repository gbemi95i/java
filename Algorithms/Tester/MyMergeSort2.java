public class myMergeSort2{
	/*public static void main(String[] args){
		int[] nums={36,8,4,97,3,91,10,3,4,37,59,2,5,67,21,45,4,97,3,91,10,3,4,37,5,1,2,3,4,6,4,37};
		int[] nums2={38,27,43,3,9,82,10,1};
		int[][] broken=arrSplit(nums);
		
		sortArrays(nums);
		
		for(int i : nums){
			System.out.println(i);
		}
	}*/
	
	public static int[][] arrSplit(int[] arr){
		int[][] singledElements=new int[arr.length][1];
		for(int i=0; i<arr.length; i++){
			int[] singleValue=new int[1];
			singleValue[0]=arr[i];
			singledElements[i]=singleValue;
		}
		
		return singledElements;
	}
		
	public static void sortArrays(int[] broken){
		int t=0;
		for(int i=0; i<broken.length; i++){
			//System.out.println(broken[i][0]);
			if(i!=broken.length-1){
				int a=broken[i];
				int b=broken[i+1];
				if(a>b){
					//System.out.println("Got Here");
					broken[i]=b;
					broken[i+1]=a;
					sortArrays(broken);
				}
			}
		}
		
		
	}
	
	public static void print(String str){System.out.println(str);}
}