class BicyclePrivatePublic{
	static int cadence=0;
	static int gear=1;
	static int speed=0;
	private int secret=55;
	
	void changeCadence(int newValue){
		cadence=newValue;
	}
	
	void speedUp(int increment){
		speed+=increment;
	}
	
	void applyBrakes(int decrement){
		speed-=decrement;
	}
	
	void changeGear(int newValue){
		gear=newValue;
	}
	
	void printStates(){
		System.out.println("Cadence:"+cadence+"\n"+
			"Speed:"+speed+"\n"+
			"Gear:"+gear
		);
	}
	
	int getSecret(){
		return secret;
	}
}