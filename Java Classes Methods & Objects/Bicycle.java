class Bicycle{
	int cadence=0;
	int speed=0;
	int gear=1;
	
	void changeCadence(int newValue){
		cadence=newValue;
	}
	
	void speedUp(int increment){
		speed+=increment;
	}
	
	void applyBrakes(int decrement){
		speed-=decrement;
	}
	
	void changeGear(int newValue){
		gear=newValue;
	}
	
	void printStates(){
		System.out.println("Cadence:"+cadence+"\n"+
			"Speed:"+speed+"\n"+
			"Gear:"+gear
		);
	}
}