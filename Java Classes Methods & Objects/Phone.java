class Phone implements AndroidInterface{
	public void openOnTap(){
		System.out.println("App opened on tap");
	}
	
	public Phone convert(Object obj){
		Phone phone=(Phone) obj;
		return phone;
	}
	
	public void call(){
		System.out.println("I win");
	}
	
	public static void call3(){
		AndroidInterface.call2();
		System.out.println(AndroidInterface.x);
	}
	
	public static void call4(){
		System.out.println("AWAY!");
	}
}