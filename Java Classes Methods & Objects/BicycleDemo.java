class BicycleDemo{
	public static void main(String[] args){
		Bicycle bmx=new Bicycle();
		Bicycle mtb=new Bicycle();
		
		bmx bm2;
		
		bmx.changeCadence(10);
		bmx.speedUp(20);
		bmx.applyBrakes(5);
		
		mtb.changeCadence(15);
		mtb.speedUp(30);
		mtb.applyBrakes(5);
		
		bmx.printStates();
		mtb.printStates();
	}
}