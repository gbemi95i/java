public class Shadowing{
	int x=5;
	static int y=6;
	class Local{
		int x=10;
		void doThis(){
			int x=15;
			System.out.println("Testing Outer X: "+Shadowing.this.x);
			System.out.println("Testing Method X: "+this.x);
			System.out.println("Testing Inner X: "+x);
		}
	}
	
	static class Local2{
		static int y=12;
		public static void doThis(){
			//REMEMBER THE 'THIS' KEYWORD ONLY WORKS WITH NON-STATIC MEMBERS...THEREFORE:
			System.out.println("Testing Outer Y: "+Shadowing.y);
			//System.out.println("Testing Method Y: "+this.y);
			System.out.println("Testing Inner X: "+y);
		}
	}
}