public class BasicMethod{
	public static void main(String[] args){
		System.out.println("5+8="+sum(5,8));
		System.out.println("3+1="+sum(3,1));
		System.out.println("23+7="+sum(23,7));
		System.out.println("14+13="+sum(14,13));
	}
	
	static int sum(int x,int y){
		return x+y;
	}
}