class ReturnObject{
	public static void main(String[] args){
		Bicycle bmx=new Bicycle();
		bmx=allocToObject(bmx,10,20,5);
		bmx.printStates();
	}
	
	public static Bicycle allocToObject(Bicycle x, int cadence, int speedIncrease, int brakeForce){
		x.changeCadence(cadence);
		x.speedUp(speedIncrease);
		x.applyBrakes(brakeForce);
		return x;
	}
}