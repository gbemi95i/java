public class CarMaker{
	interface CarMake{
		public void addMake(String make);
		public void addModel(String model);
		public void addYear(int year);
		public int getYear();
	}
	
	public static void main(String[] args){
		CarMake car=getCar();
		car.addMake("Mercedes");
		car.addModel("E300");
		car.addYear(2016);
		print(car.getYear());
	}
	
	public static CarMake getCar(){
		CarMake car=new CarMake(){
			String make;
			String model;
			int year;
			
			public void addMake(String make){
				this.make=make;
			};
			public void addModel(String model){
				this.model=model;
			};
			public void addYear(int year){
				this.year=year;
				print("Got here");
			};
			public int getYear(){
				return year;
			};
		};
		
		return car;
	}
	
	public static void print(int x){System.out.println(x);}
	public static void print(char x){System.out.println(x);}
	public static void print(String x){System.out.println(x);}
}