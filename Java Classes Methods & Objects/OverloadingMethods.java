import java.util.Arrays;
public class SameNameMethods{
	public static void main(String[] args){
		int a[]={1,4,7};
		int b[]={2,5,8};
		System.out.println("a = "+Arrays.toString(a)+"; b = "+Arrays.toString(b));
		doThis("Baby");
		doThis(555);
		doThis('B');
	}
	
	public static void doThis(String y){
		print(y);
		//return;
	}
	
	public static void doThis(int y){
		print(y);
		//return;
	}
	
	public static void doThis(char y){
		print(y);
		//return;
	}
	public static void print(int x){System.out.println(x);}
	public static void print(char x){System.out.println(x);}
	public static void print(String x){System.out.println(x);}
}