class BicycleDemo2{
	public static void main(String[] args){
		Bicycle bmx=new Bicycle();
		Bicycle mtb=new Bicycle();
		BicyclePrivatePublic test = new BicyclePrivatePublic();
		BicyclePrivatePublic test2 = new BicyclePrivatePublic();
		
		bmx.changeCadence(10);
		bmx.speedUp(20);
		bmx.applyBrakes(5);
		
		mtb.changeCadence(15);
		mtb.speedUp(30);
		mtb.applyBrakes(5);

		test.changeCadence(15);
		test.speedUp(30);
		test.applyBrakes(5);
		
		//bmx.printStates();
		//mtb.printStates();
		//test2.printStates();
		test2.printStates();//THE SAME SINCE THE VARIABLES IN BicyclePrivatePublic ARE STATIC
		
		//System.out.println("Accessing test's private variable:"+test.secret);//BECAUSE THE SECRET FIELD WAS DECLARED PRIVATE, THIS WOULD PRODUCE AN ERROR
																//PRIVATE FIELDS CAN ONLY BE ACCESSED BY A METHOD...FOR INSTANCE:
		System.out.println("Accessing test's private variable via a method:"+test.getSecret());
	}
}