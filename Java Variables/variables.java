public class variables{
	public static void main(String[] args){
		//IN JAVA IT'S ONLY BOOLEAN NOTHING LIKE BOOL
		//A CHAR VARIABLE VALUE CAN ONLY BE IN '' NOT ""
		//A LONG IS A 64bit SIGNED TWO COMPLEMENT INTEGER
		
		int i=45;
		String s="bla";
		char c='C';
		boolean b=false;
		float f=5.56f;
		double d=4.35d;
		short sh=15000;
		long l=7L;
		System.out.println(i);
		System.out.println(s);
		System.out.println(c);
		System.out.println(b);
		System.out.println(f);
		System.out.println(d);
		System.out.println(sh);
		System.out.println(l);
		System.out.println("All="+i+", "+s+", "+c+", "+b+", "+f+", "+d+", "+sh+", "+l);
	}
}